#!/usr/bin/env bash

# ================================================================================
# REQUIRED SETTINGS
# ================================================================================

export APPLICATION_NAME="___APPLICATION_NAME___"
export APPLICATION_ENV="prod"
export APPLICATION_ENCODING="UTF-8"
export APPLICATION_LOCALE="en_US"
export APPLICATION_TIMEZONE="America/New_York"
export DEBUG="false"

# ================================================================================
# RUNTIME CONFIGURATION (optional)
#
# These settings enable the application's Configuration classes to reach out to
# a 3rd-party provider (such as AWS SSM Parameter Store) to obtain configuration
# values in place of them being hard-coded in a config file. This allows apps to
# be configured on-the-fly or without requiring a redeployment.
# ================================================================================

export ENABLE_RUNTIME_CONFIG=0
export RUNTIME_CONFIG_AWS_REGION="us-east-2"

# ================================================================================
# Logging
# ================================================================================

export LOG_DEBUG_URL="file://logs/?levels[]=notice&levels[]=info&levels[]=debug&file=debug"
export LOG_ERROR_URL="file://logs/?levels[]=warning&levels[]=error&levels[]=critical&levels[]=alert&levels[]=emergency&file=error"

# ================================================================================
# SECURITY
# Tip: Leave commented out to use runtime configuration!
# ================================================================================

#export SECURITY_SALT="__SALT__"

# ================================================================================
# Database
# Tip: Leave commented out to use runtime configuration!
# ================================================================================

#export DB_HOST="localhost"
#export DB_PORT="3306"
#export DB_USERNAME="root"
#export DB_PASSWORD=""
#export DB_NAME="mywebsite"

# ================================================================================
# Caching
# ================================================================================

export CACHE_DURATION="+10 minutes"
export CACHE_DEFAULT_URL="file://tmp/cache/?prefix=${APPLICATION_NAME}_default&duration=${CACHE_DURATION}"
export CACHE_CAKECORE_URL="file://tmp/cache/persistent?prefix=${APPLICATION_NAME}_cake_core&serialize=true&duration=${CACHE_DURATION}"
export CACHE_CAKEMODEL_URL="file://tmp/cache/models?prefix=${APPLICATION_NAME}_cake_model&serialize=true&duration=${CACHE_DURATION}"

# ================================================================================
# API keys
# Tip: Leave commented out to use runtime configuration!
# ================================================================================

# Google analytics
#export GOOGLE_ANALYTICS_CLIENT_ID=""
#export GOOGLE_ANALYTICS_CLIENT_SECRET=""
#export GOOGLE_ANALYTICS_KEY_FILE_NAME=""
#export GOOGLE_ANALYTICS_GLOBAL_SITE_TAG=""
#export GOOGLE_ANALYTICS_MAIN_VIEW_ID=""

# Google Recaptcha
#export GOOGLE_RECAPTCHA_SITE_KEY=""
#export GOOGLE_RECAPTCHA_SECRET_KEY=""

# Mailgun
#export MAILGUN_API_KEY=""

# Zapier
#export ZAPIER_WEBHOOK_URL=""
