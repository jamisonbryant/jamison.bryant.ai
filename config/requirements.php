<?php

/**
 * Requirements Checker
 *
 * Checks that the server meets the minimum requirements to run CakePHP.
 *
 * @package   None
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2018 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 */

// Check PHP version
if (version_compare(PHP_VERSION, '5.6.0') < 0) {
    trigger_error('Your PHP version must be equal or higher than 5.6.0 to use CakePHP.' . PHP_EOL, E_USER_ERROR);
}

// Check if intl extension installed
if (!extension_loaded('intl')) {
    trigger_error('You must enable the intl extension to use CakePHP.' . PHP_EOL, E_USER_ERROR);
}

// Check if mbstring extension is installed
if (!extension_loaded('mbstring')) {
    trigger_error('You must enable the mbstring extension to use CakePHP.' . PHP_EOL, E_USER_ERROR);
}
