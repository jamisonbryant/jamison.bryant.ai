<?php

/**
 * Application Bootstrapper
 *
 * Performs the various setup operations that the CakePHP framework needs to do
 * in order to function, including registering the Cake autoloader and setting
 * default application paths.
 *
 * @package   None
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2020 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 */

// Load CakePHP-specific paths and general filesystem paths
require __DIR__ . DS . 'paths.php';

// Don't start if sensitive files are deployed
if (file_exists(ROOT . DS . '.sensitive')) {
    trigger_error('Security failure' . PHP_EOL, E_USER_ERROR);
}

// Load CakePHP core bootstrapper
require CORE_PATH . 'config' . DS . 'bootstrap.php';

// Load env file
if (!file_exists(CONFIG . '.env')) {
    trigger_error('.env file not found.' . PHP_EOL, E_USER_ERROR);
}

$dotenv = new \josegonzalez\Dotenv\Loader([CONFIG . '.env']);
$dotenv->parse()->putenv()->toEnv()->toServer();

// Load imports
use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Security;

/**
 * Loads application config files
 */
function loadConfigFiles()
{
    try {
        Configure::config('default', new PhpConfig());
        Configure::load('app', 'default', false);
    } catch (\Exception $e) {
        exit($e->getMessage() . "\n");
    }
}

function configureMetadataCache()
{
    $expire_fast = '+2 minutes';
    $expire_faster = '+2 seconds';

    // Configure metadata cache
    // (in debug mode, the cache should only last for a short time)
    if (Configure::read('debug')) {
        Configure::write('Cache._cake_model_.duration', $expire_fast);
        Configure::write('Cache._cake_core_.duration', $expire_fast);

        // Disable router cache entirely during development
        Configure::write('Cache._cake_routes_.duration', $expire_faster);
    }
}

/**
 * Sets the application timezone and locale
 */
function setTimezoneAndLocale()
{
    // Set default server timezone
    date_default_timezone_set(Configure::read('App.defaultTimezone'));

    // Configure mbstring extension
    mb_internal_encoding(Configure::read('App.encoding'));

    // Set default locale
    ini_set('intl.default_locale',
        Configure::read('App.defaultLocale'));
}

/**
 * Registers application error and exception handlers
 */
function registerErrorHandlers()
{
    // Determine if PHP is running in CLI mode
    $isCli = PHP_SAPI === 'cli';

    // Register appropriate error handler for CLI
    if ($isCli) {
        (new ConsoleErrorHandler(Configure::read('Error')))->register();
    } else {
        (new ErrorHandler(Configure::read('Error')))->register();
    }

    // Include CLI bootstrap overrides
    if ($isCli) {
        require __DIR__ . '/bootstrap_cli.php';
    }
}

/**
 * Sets the application full base URL
 *
 * The base URL is used as the base of all absolute links. (note: if the full
 * base URL is defined in the application config file this method can be
 * removed).
 */
function setFullBaseURL()
{
    if (!Configure::read('App.fullBaseUrl')) {
        $protocol = 'http';
        $http_host = env('HTTP_HOST');

        if (env('HTTPS')) {
            $protocol .= 's';
        }

        if (isset($http_host)) {
            Configure::write('App.fullBaseUrl',
                sprintf('%s://%s', $protocol, $http_host));
        }
    }
}

/**
 * Applies all loaded configuration to the application
 *
 * All configuration settings that are loaded to this point in the bootstrapping
 * process will be applied to the application after this method runs.
 */
function applyConfiguration()
{
    Cache::setConfig(
        Configure::consume('Cache')
    );

    ConnectionManager::setConfig(
        Configure::consume('Datasources')
    );

    TransportFactory::setConfig(
        Configure::consume('EmailTransport')
    );

    Email::setConfig(
        Configure::consume('Email')
    );

    Log::setConfig(
        Configure::consume('Log')
    );

    Security::setSalt(
        Configure::consume('Security.salt')
    );
}

/*
 * Sets up detectors for mobiles and tablets
 */
function setupDeviceDetectors()
{
    $detector = new \Detection\MobileDetect();

    ServerRequest::addDetector('mobile', function ($request) use ($detector) {
        $detector->isMobile();
    });

    ServerRequest::addDetector('tablet', function ($request) use ($detector) {
        $detector->isTablet();
    });
}

/**
 * Enables immutable time objects in the ORM
 */
function enableImmutableTimes()
{
    Type::build('time')->useImmutable();
    Type::build('date')->useImmutable();
    Type::build('datetime')->useImmutable();
    Type::build('timestamp')->useImmutable();
}

/**
 * All together now!
 */
function main()
{
    loadConfigFiles();
    configureMetadataCache();
    setTimezoneAndLocale();
    registerErrorHandlers();
    setFullBaseURL();
    applyConfiguration();
    setupDeviceDetectors();
    enableImmutableTimes();
}

// Call main
main();
