<?php

/**
 * Application URL Routes
 *
 * Defines the routes that the URL router recognizes and maps URL patterns to
 * controller actions.
 *
 * @package   None
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2018 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 */

use Cake\Core\Configure;
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

// Set default route class
Router::defaultRouteClass(DashedRoute::class);

// Set master routes
Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    // Apply middleware to current route scope
    $routes->applyMiddleware('csrf');

    // Connect pages routes
    // By switching on the configuration variable containing the site status, we either set up default page rendering
    // routes or a catch-all route to display the appropriate shade page (coming soon or down for maintenance)
    switch (Configure::read('status')) {
        default:
        case 'up':
            $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
            $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
            break;

        case 'down':
            $routes->connect('*', ['controller' => 'Pages', 'action' => 'shades', 'down-for-maintenance']);
            break;

        case 'coming':
            $routes->connect('*', ['controller' => 'Pages', 'action' => 'shades', 'coming-soon']);
            break;
    }

    // Connect catchall routes
    $routes->fallbacks(DashedRoute::class);
});

// Set admin routes
Router::prefix('admin', function ($routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->fallbacks(DashedRoute::class);
});

// Set API routes
Router::scope('/api', function ($routes) {
    $routes->setExtensions(['json']);

    $routes->resources('Downloads', [
        'map' => [
            'list' => [
                'action' => 'list',
                'method' => 'GET',
            ],
        ],
    ]);
});

