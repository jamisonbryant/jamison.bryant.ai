<?php

/**
 * Application Paths
 *
 * Defines paths and path names as constants which are used throughout the app
 * for resource loading and linking.
 *
 * @package   None
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2018 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 */

// Directory separator constant (usually /)
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

// Full path to directory containing "src" (NO trailing DS)
define('ROOT', dirname(__DIR__));

// Name of application directory (usually "src")
define('APP_DIR', 'src');

// Path to application directory
define('APP', ROOT . DS . APP_DIR . DS);

// Path to config directory
define('CONFIG', ROOT . DS . 'config' . DS);

// Path to webroot directory
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

// Path to public images directory
define('IMG_ROOT', ROOT . DS . 'webroot' . DS . 'img' . DS);

// Path to tests directory
define('TESTS', ROOT . DS . 'tests' . DS);

// Path to temporary directory
define('TMP', ROOT . DS . 'tmp' . DS);

// Path to logs directory
define('LOGS', ROOT . DS . 'logs' . DS);

// Path to cache directory
define('CACHE', TMP . 'cache' . DS);

// Path to "cake" directory (NO trailing DS)
define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');

// Path to "cake" directories (WITH trailing DS)
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);
