<?php

use App\Config\ConfigurationResolver;


try {
    if (env('ENABLE_RUNTIME_CONFIG', 0) == 1) {
        $resolver = new ConfigurationResolver(
            env('APPLICATION_NAME'),
            env('APPLICATION_ENV'),
            env('RUNTIME_CONFIG_AWS_REGION')
        );
    } else {
        $resolver = new ConfigurationResolver(
            env('APPLICATION_NAME'),
            env('APPLICATION_ENV')
        );
    }
} catch (TypeError $e) {
    trigger_error('Unable to bootstrap runtime configuration resolver.' . PHP_EOL, E_USER_ERROR);
}

return [
    /*
     * Debug level
     *
     * Possible values:
     *  - false (debug mode disabled): no error messages or warnings shown
     *  - true (debug mode enabled): errors and warnings shown
     */
    'debug' => $resolver->fetch('DEBUG', false),

    /*
     * Basic application configuration
     *
     * Settings:
     *  - namespace: default namespace to find app classes under
     *  - defaultLocale: default locale for translation and formatting
     *  - encoding: encoding to use for HTML and database connections
     *  - base: base directory application resides in (if false, will be auto-detected)
     *  - dir: name of the app/ directory
     *  - webroot: name of the webroot/ directory
     *  - wwwRoot: file path to the webroot
     *  - baseUrl: used to disable mod_rewrite
     *  - fullBaseUrl: base URL to use for absolute links (if false, will be auto-generated)
     *  - imageBaseUrl: web path to the public images directory in the webroot
     *  - cssBaseUrl: web path to the public css directory in the webroot
     *  - jsBaseUrl: web path to the public js directory in the webroot
     *  - paths: paths for non-class-based resources
     */
    'App' => [
        'namespace' => 'App',
        'encoding' => $resolver->fetch('APPLICATION_ENCODING', 'UTF-8'),
        'defaultLocale' => $resolver->fetch('APPLICATION_LOCALE', 'en_US'),
        'defaultTimezone' => $resolver->fetch('APPLICATION_TIMEZONE', 'America/New_York'),
        'base' => false,
        'dir' => 'src',
        'webroot' => 'webroot',
        'wwwRoot' => WWW_ROOT,
        //'baseUrl' => $resolver->fetch('SCRIPT_NAME'),
        'fullBaseUrl' => false,
        'imageBaseUrl' => 'img/',
        'cssBaseUrl' => 'css/',
        'jsBaseUrl' => 'js/',
        'paths' => [
            'plugins' => [ROOT . DS . 'plugins' . DS],
            'templates' => [APP . 'Template' . DS],
            'locales' => [APP . 'Locale' . DS],
        ],
    ],

    /*
     * Security and encryption configuration
     *
     * Settings:
     *  - salt - A random string used in security hashing methods
     */
    'Security' => [
        'salt' => $resolver->fetchRequired('SECURITY_SALT'),
    ],

    /*
     * Apply timestamps with the last modified time to static assets (js, css, images).
     * Will append a querystring parameter containing the time the file was modified.
     * This is useful for busting browser caches.
     *
     * Set to true to apply timestamps when debug is true. Set to 'force' to always
     * enable timestamping regardless of debug value.
     */

    /*
     * Asset configuration
     *
     * Settings:
     *  - timestamp: enable timestamping on assets to bust caches
     *  - cacheTime: time to keep assets in cache
     */
    'Asset' => [
        //'timestamp' => true,
        // 'cacheTime' => '+1 year'
    ],

    /*
     * Cache configuration
     *
     * Settings (per provider):
     *  - className: full classname of class providing caching
     *  - prefix: prefix to use for cache files/entries
     *  - duration: length of time to keep data in cache
     *
     * NOTE: Configuration settings vary by cache provider, check the CakePHP
     * docs for more information on cache provider configurations.
     */
    'Cache' => [
        'default' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'path' => CACHE,
            'url' => null,
        ],

        // General framework caching
        '_cake_core_' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'prefix' => '_cake_core_',
            'path' => CACHE . 'persistent/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => null,
        ],

        // Model and datasource caching
        '_cake_model_' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'prefix' => '_cake_model_',
            'path' => CACHE . 'models/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => null,
        ],

        // Route caching
        '_cake_routes_' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'prefix' => '_cake_routes_',
            'path' => CACHE,
            'serialize' => true,
            'duration' => '+1 years',
            'url' => null,
        ],

        // Cell caching
        'cells' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'prefix' => 'cell_',
            'path' => CACHE . 'cells/',
            'serialize' => true,
            'duration' => '+7 days',
            'url' => null,
        ],
    ],

    /*
     * Error and exception configuration
     *
     * Settings:
     *  - errorLevel: level of errors to capture
     *  - trace: whether or not backtraces should be included in logged errors/exceptions
     *  - log: whether or not to log exceptions
     *  - exceptionRenderer: class responsible for rendering uncaught exceptions
     *  - skipLog: list of exceptions to skip for logging
     *  - extraFatalErrorMemory: number of megabytes to increase the memory limit by when a fatal error is encountered
     */
    'Error' => [
        'errorLevel' => E_ALL,
        'exceptionRenderer' => 'Cake\Error\ExceptionRenderer',
        'skipLog' => [],
        'log' => true,
        'trace' => true,
    ],

    /*
     * Email configuration
     *
     * Settings (per transport):
     *  - className: name of class providing email transport
     *
     * NOTE: Configuration settings vary by email transport, check the CakePHP
     * docs for more information on email transport configurations.
     */
    'EmailTransport' => [
        'default' => [
            'className' => 'Cake\Mailer\Transport\MailTransport',
            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'username' => null,
            'password' => null,
            'client' => null,
            'tls' => null,
            'url' => null,
        ],
    ],

    /*
     * Email profiles configuration
     *
     * Settings (per profile):
     *  - transport: transport to use for this profile
     *  - from: default from address
     *  - charset: character set to use when building email
     *  - headerCharset: character set to specify in the email header
     *
     * NOTE: Configuration settings vary by email transport, check the CakePHP
     * docs for more information on email transport configurations.
     */
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'you@localhost',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
    ],

    /*
     * Datasource configuration
     *
     * Settings (per datasource):
     *  - className: name of class providing datasource connection
     *  - driver: name of class providing query interface
     *  - persistent: whether or not to use a persistent connection
     *  - url: datasource URL/URI
     *
     * NOTE: Configuration settings vary by datasource type, check the CakePHP
     * docs for more information on datasource configurations.
     */
    'Datasources' => [
        // TODO: Ensure we're using utf8mb4 and not latin1 encoding/collation
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => $resolver->fetchRequired('DB_HOST'),
            'port' => $resolver->fetch('DB_PORT', 3306),
            'username' => $resolver->fetchRequired('DB_USERNAME'),
            'password' => $resolver->fetchRequired('DB_PASSWORD'),
            'database' => $resolver->fetchRequired('DB_NAME'),
            //'encoding' => 'utf8mb4',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => null,
        ],

        // TODO: Use sqlite for testing database so we don't have to setup MySQL in CI pipeline
        'test' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => 'test_myapp',
            //'encoding' => 'utf8mb4',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            'log' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => null,
        ],

        /*
         * NOTE: Do NOT use the configuration resolver for this connection! Migrations should be
         * run MANUALLY, by a developer or architect, using the command identified in the runbook.
         *
         * Reasoning: Migrations occasionally need to CREATE and/or DROP tables/records, and the
         * default app user doesn't have these permissions. The master user, however...
         */
        'migrations' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => env('MIGRATIONS_DB_HOST'),
            //'port' => 'non_standard_port_number',
            'username' => env('MIGRATIONS_DB_USERNAME'),
            'password' => env('MIGRATIONS_DB_PASSWORD'),
            'database' => env('MIGRATIONS_DB_NAME'),
            //'encoding' => 'utf8mb4',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            'log' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => null,
        ],
    ],

    /*
     * Logging configuration
     *
     * Settings (per logger):
     *  - className: name of class providing log handling
     *  - scopes: array of scopes that logger should handle
     *  - levels: array of log levels the logger accepts
     */
    'Log' => [
        // Debug logger
        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'debug',
            'scopes' => false,
            'levels' => ['notice', 'info', 'debug'],
        ],

        // Error logger
        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'error',
            'scopes' => false,
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
        ],

        // AWS logger
        'aws' => [
            'className' => 'App\Log\Engine\AWSLog',
            'path' => LOGS,
            'file' => 'aws',
            'scopes' => ['aws'],
            'levels' => [],
        ],

        // Query logger
        // (to enable this dedicated query log, must set datasource's log flag to true)
        'queries' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'queries',
            'url' => null,
            'scopes' => ['queriesLog'],
        ],
    ],

    /*
     * Session configuration
     *
     * Settings:
     *  - cookie: name of the cookie to use (defaults to 'CAKEPHP')
     *  - cookiePath: URL path for which session cookie is set (defaults to base path of app)
     *  - timeout: time in minutes session should be valid for
     *  - defaults: default configuration set to use as a basis for the session
     *  - handler: custom session handler configuration
     *  - ini: array of additional INI values to set
     */
    'Session' => [
        'defaults' => 'php',
    ],

    /*
     * API keys
     */
    'Api' => [
        'GoogleAnalytics' => [
            'clientId' => $resolver->fetch('GOOGLE_ANALYTICS_CLIENT_ID'),
            'clientSecret' => $resolver->fetch('GOOGLE_ANALYTICS_CLIENT_SECRET'),
            'keyFileName' => $resolver->fetch('GOOGLE_ANALYTICS_KEY_FILE_NAME'),
            'globalSiteTag' => $resolver->fetch('GOOGLE_ANALYTICS_GLOBAL_SITE_TAG'),
            'viewIds' => [
                'allWebSiteData' => $resolver->fetch('GOOGLE_ANALYTICS_MAIN_VIEW_ID'),
            ],
        ],
        'GoogleRecaptcha' => [
            'siteKey' => $resolver->fetch('GOOGLE_RECAPTCHA_SITE_KEY'),
            'secretKey' => $resolver->fetch('GOOGLE_RECAPTCHA_SECRET_KEY'),
        ],
        'Mailgun' => [
            'apiKey' => $resolver->fetch('MAILGUN_API_KEY'),
        ],
        'Zapier' => [
            'shadeFormWebhookUrl' => $resolver->fetch('ZAPIER_WEBHOOK_URL'),
        ],
    ],

    /*
     * Feature flags
     */
    'Features' => [
        'projectsModule' => [
            'dev' => false,
            'test' => false,
            'prod' => false,
        ],
        'siteUpdatesModule' => [
            'dev' => false,
            'test' => false,
            'prod' => false,
        ],
        'interestsPage' => [
            'dev' => false,
            'test' => false,
            'prod' => false,
        ],
    ],

    /*
     * Links
     */
    'Links' => [
        'social' => [
            'blog' => 'https://serioussemicolons.wordpress.com',
            'twitter' => 'https://twitter.com/AethridODauth',
            'linkedin' => 'https://www.linkedin.com/in/jamisonbryant/',
            'gitlab' => 'https://gitlab.com/jamisonbryant',
            'stackoverflow' => 'https://stackoverflow.com/users/10882087/jamison-bryant',
            'codepen' => 'https://codepen.io/jamisonbryant',
        ],
        'resume' => [
            'pdf' => 'https://drive.google.com/file/d/1bMCCSFv6d3nOsX883l9EXBX5B0HbjPL2/view?usp=sharing',
            'docx' => 'https://docs.google.com/document/d/1MLD4dQwP9pdnriuq7ufAoDw_NEMlIh7T/edit?usp=sharing&ouid=104870491574993187089&rtpof=true&sd=true',
            'html' => 'https://drive.google.com/file/d/1Tdco277iBs7XRmnq8QaDT9FGuNjG0wAD/view?usp=sharing',
            'rtf' => 'https://drive.google.com/file/d/1OPxHSNTyMMcXQRqVXvKuEsuxs8_DvGe-/view?usp=sharing',
            'odt' => 'https://drive.google.com/file/d/1AQC09e-t_ezi04lJUCZHuDq-ahHaRyrS/view?usp=sharing',
            'epub' => 'https://drive.google.com/file/d/1kybzY8KjvSMzyQwAJfhJu14GKH0b_IFz/view?usp=sharing',
            'txt' => 'https://drive.google.com/file/d/1qwUjydWorZgqAAHLO_k7_TWx4nhVBeKi/view?usp=sharing',
        ]
    ],

    /*
     * Statuses
     */
    'Status' => [
        'employment' => 'EMPLOYED',
        'contracting' => 'BOOKED',
        'speaking' => 'BOOKED',
    ],
];
