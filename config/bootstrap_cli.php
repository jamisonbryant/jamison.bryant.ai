<?php

/**
 * Application Bootstrapper (CLI only)
 *
 * Performs the various setup operations that the CakePHP framework needs to do
 * in order to function, including registering the Cake autoloader and setting
 * default application paths.
 *
 * NOTE: This file is loaded only when the application is running in CLI mode.
 *
 * @package   None
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2018 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 */

use Cake\Core\Configure;

/*
 * Put additional bootstrapping and configuration for CLI environments here
 */

// Set the fullBaseUrl to allow URLs to be generated in shell tasks.
// This is useful when sending email from shells.
//Configure::write('App.fullBaseUrl', php_uname('n'));

// Set logs to different files so they don't have permission conflicts
Configure::write('Log.debug.file', 'cli-debug');
Configure::write('Log.error.file', 'cli-error');
