/* global  $ */

/**
 * ActiveNav.js
 *
 * Applies the 'active' class to the appropriate link in a navigation control based
 * on the current page's URL. Works on any link with the Bootstrap class .nav-link
 * UNLESS the link also has the .activenav-ignore class.
 *
 * @author Jamison Bryant <jamison@bryant.ai>
 * @copyright 2020 Jamison Bryant. ALl rights reserved.
 * @since v0.1.0
 */
$(document).ready(function() {
    const current = location.pathname;

    $('.nav-link:not(.activenav-ignore)').each(function() {
        const $this = $(this);

        if ($this.attr('href') === current) {
            $this.addClass('active');
        }
    });
});
