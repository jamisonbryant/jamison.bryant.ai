/* global  $ */
/* global  feather */

/**
 * FeatherIcons.js
 *
 * Triggers the Feather icon library companion script to replace <i>s with SVGs so icons get rendered.
 * Use this instead of Font Awesome whenever possible, because FA doesn't use SVGs.
 *
 * @author Jamison Bryant <jamison@bryant.ai>
 * @copyright 2020 Jamison Bryant. ALl rights reserved.
 * @since v0.2.2
 */
$(document).ready(function() {
    if ($('i[data-feather]').length > 0) {
        feather.replace();
    }
});
