/* global  $ */

// for now, until I add some logic
/* eslint-disable no-unused-vars */

const doNotTrackMessage = `
You have "Do Not Track" enabled in your browser! I will respect your wishes and not collect analytics on your visit.
All I use them for is to see what kind of devices are accessing my site and which pages people visit so that I can
provide the best service possible :)
---
COMPLIANCE NOTICE: If you previously had DnT ~off~ and wish to have your information removed from my site (or for any
other reason), email me at jamison+do-not-track-me [at] bryant [dot] ai!
`;

const enabledModules = [
    'saveRequest',
    'saveDeviceType',
    'saveFileActivity',
    'saveResumeActivity',
    'saveClickout'
];

$(document).ready(function() {
    if (navigator.doNotTrack === 1) {
        console.info(doNotTrackMessage);
        return;
    }

    setupAPIClients();
    enabledModules.filter(f => Object.prototype.hasOwnProperty.call(window, f)).forEach(func => window[func]());
});

function setupAPIClients() {
    // need to call out to an api endpoint from within here
}

function getAPIEndpointURL() {
    // for getting the exact HTTP endpoint URI for an API call
}

function saveRequest() {
    // saves details about the request (page, status code, ip address)
}

function saveDeviceType() {
    // saves details about the browser and hardware (from user agent)
}

function saveFileActivity() {
    // if viewing or downloading a file, saves that
}

function saveResumeActivity() {
    // if viewing or downloading my resume, saves that
}

function saveClickout() {
    // if leaving my website for another of my websites, saves that
}
