/* eslint-disable no-unused-vars */

/**
 * "Wait for Global" function
 * A simple utility function you pass a string and a callback to, and the call back will be called when (if ever)
 * it is available as a global object. Useful for creating objects from 3rd-party libraries only once you know the
 * scripts have loaded from the CDN, such as with Chart.js and Chart() objects.
 *
 * Usage:
 *   waitForGlobal("jQuery", function() {
 *       console.log("ready to use");
 *   });
 */
function waitForGlobal(key, callback) {
    if (window[key]) {
        callback();
    } else {
        setTimeout(function() {
            waitForGlobal(key, callback);
        }, 100);
    }
}

/* eslint-disable no-unused-vars */
