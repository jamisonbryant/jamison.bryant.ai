# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/webroot/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/webroot/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/webroot/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/webroot/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/webroot/favicon-16x16.png">
    <link rel="manifest" href="/webroot/site.webmanifest">
    <link rel="mask-icon" href="/webroot/safari-pinned-tab.svg" color="#3d4780">
    <link rel="shortcut icon" href="/webroot/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Jamison Bryant">
    <meta name="application-name" content="Jamison Bryant">
    <meta name="msapplication-TileColor" content="#3d4780">
    <meta name="msapplication-config" content="/webroot/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)