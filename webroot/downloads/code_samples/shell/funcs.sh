
##
# Checks if the system is currently needs a reboot
##
function is_reboot_pending {
  if [ -f /var/run/reboot-required ]; then
    return true
  else
    return false
  fi
}

##
# Reloads an Apache Vhost and restarts Apache to apply configuration changes
##
function reload_apache_vhost {
  if [ ! -z $1 ]; then
    echo "INFO: Reloading Apache Vhost $1..."
      a2dissite $1
      a2ensite $1
      service apache2 reload
  else
    echo "ERROR: Must specify name of virtual host name to reload"
  fi
}

##
# Displays information on the system's battery (charge state, time to full, etc.)
##
function display_battery_info {
  if hash upower 2>/dev/null; then
    upower -i $(upower -e | grep 'BAT') | grep -E "state|time\ to\ full|percentage"
  else
    echo "ERROR: upower is required to run this command"
  fi
}

##
# Sets the appropriate permissions on /tmp and /logs for a CakePHP application
##
function fix_cakephp_permissions {
  HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
  setfacl -R -m u:${HTTPDUSER}:rwx tmp
  setfacl -R -d -m u:${HTTPDUSER}:rwx tmp
  setfacl -R -m u:${HTTPDUSER}:rwx logs
  setfacl -R -d -m u:${HTTPDUSER}:rwx logs
}

##
# Generates a random security key for a CakePHP application
##
function generate_cakephp_security_key {
  cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1
}

##
# Sets the permissions of the user's SSH files to the right values
##
function fix_ssh_permissions {
  chmod 700 ~/.ssh
  chmod 644 ~/.ssh/id_rsa.pub
  chmod 600 ~/.ssh/id_rsa
}

##
# Colorizes man page output
##

man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
            man "$@"
}

export FUNCTIONS_CREATED=true