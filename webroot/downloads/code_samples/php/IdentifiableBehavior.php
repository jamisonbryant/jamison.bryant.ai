
class IdentifiableBehavior extends Behavior
{
    /**
     * Generates a pseudo-random 13-character base 64 ID.
     */
    public function generateBase64Id()
    {
        // Get random base 10 number between 0 and mt_getrandmax()
        $base10 = mt_rand() * mt_rand();

        // Convert random base 10 number to base 16
        $base16 = base_convert($base10, 10, 16);

        // Base 64 encode base 16 number
        $base64 = base64_encode(pack('H*', $base16));

        // Replace unsafe URL characters (/ and +)
        $base64 = str_replace('/', '_', $base64);
        $base64 = str_replace('+', '-', $base64);

        // Remove trailing double-equals sign
        $base64 = rtrim($base64, '=');

        // Return Base 64 ID
        return $base64;
    }

    /**
     * Generates a pseudo-random RFC4211-compliant version 4 (non-name based) UUID.
     */
    public function generateUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Generates IDs for an entity before it is saved to the database.
     *
     * @param Event $event Instance of save event
     * @param EntityInterface $entity Entity being saved
     */
    public function beforeSave(Event $event, EntityInterface $entity)
    {
        // Check if entity is being created in database
        // If so, update appropriate ID fields if present
        if ($entity->isNew()) {
            $entity->set($this->config('base64.field'), $this->generateBase64Id());
            $entity->set($this->config('uuid.field'), $this->generateUuid());
        }
    }
}
