
public class SurvivoComponent
{
    /// < summary>
    /// Loads API connection parameters and prepares component for use. 
    /// < /summary>
    public SurvivoComponent()
    {
        // Capture API connection parameters from the config file.
        orgId = ConfigurationManager.AppSettings["SurvivoApiOrgId"];
        apiUrl = ConfigurationManager.AppSettings["SurvivoApiUrl"];
        apiUsername = ConfigurationManager.AppSettings["SurvivoApiUsername"];
        apiPassword = ConfigurationManager.AppSettings["SurvivoApiPassword"];
    }

    /// < summary>
    /// Checks if the component can connect to Survivo.
    /// < /summary>
    /// < returns>Returns true if connection is successful, false otherwise.< /returns>
    public bool CanConnectToSurvivo()
    {
        bool canConnect = false;

        // First check if the app can connect to the Internet
        // If it can't, it can't connect to Survivo either, so return false.
        if (NetHelper.CanConnectToInternet()) {
            // Submit basic request to Survivo API
            // API should respond with code 200 (OK)
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                {
                    { "org_id", orgId },
                    { "login_email", apiUsername },
                    { "login_pw", apiPassword },
                    { "lookup_type", "vol" },
                    { "lookup_method", "id" },
                    { "result_type", "id" },
                    { "action", "select" }
                });

                // Parse response from byte array into XML
                // Then get status code node value from XML via xpath
                lastResponse = System.Text.Encoding.UTF8.GetString(response);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(lastResponse);
                var status = xmlDoc.SelectSingleNode("/response/status/code").InnerText;

                // If the status code is 200, connection was successful.
                // Otherwise, read the error message from the response and log it.
                if (status.Equals("200")) {
                    canConnect = true;
                } else {
                    var message = xmlDoc.SelectSingleNode("/response/status/message");
                    log.Error("Unable to connect to Survivo: " + message.InnerText);
                }
            }
        } else {
            canConnect = false;
        }

        return canConnect;
    }

    /// < summary>
    /// Looks up a volunteer in the Survivo database by email address.
    /// < /summary>
    /// < param name="email">Email of volunteer to look up< /param>
    /// < returns>Volunteer object if volunteer email found< /returns>
    public Volunteer GetVolunteerFromEmail(string email)
    {
        Volunteer vol = null;

        if (CanConnectToSurvivo()) {
            // Submit lookup request to Survivo API
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                {
                    { "org_id", orgId },
                    { "login_email", apiUsername },
                    { "login_pw", apiPassword },
                    { "lookup_type", "vol" },
                    { "lookup_method", "email" },
                    { "result_type", "full" },
                    { "action", "select" },
                    { "lookup_vol_email", email }
                });

                // Parse response from byte array into XML
                // Then get status code node value from XML via xpath
                lastResponse = System.Text.Encoding.UTF8.GetString(response);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(lastResponse);

                int responseCode = 0;
                int numRecords = 0;

                if (xmlDoc.HasChildNodes) {
                    responseCode = int.Parse(xmlDoc.SelectSingleNode("//response/status/code").InnerText);
                    numRecords = int.Parse(xmlDoc.SelectSingleNode("//response/status/records").InnerText);
                }

                // Check if Survivo API returned exactly one record
                // If so, create and populate the volunteer object...otherwise, vol is null.
                if (responseCode == 200 && numRecords == 1) {
                    // Set volunteer properties from XML data
                    // One property (email) is pulled from the method parameters.
                    vol = new Volunteer()
                    {
                        Id = int.Parse(xmlDoc.SelectSingleNode("//volunteer/cervis_vol_id").InnerText),
                        FirstName = xmlDoc.SelectSingleNode("//volunteer/fname").InnerText,
                        LastName = xmlDoc.SelectSingleNode("//volunteer/lname").InnerText,
                        Email = email,
                        PrimaryPhone = xmlDoc.SelectSingleNode("//volunteer/priphone").InnerText,
                        AlternatePhone = xmlDoc.SelectSingleNode("//volunteer/altphone").InnerText,
                        StreetAddress1 = xmlDoc.SelectSingleNode("//volunteer/address").InnerText,
                        StreetAddress2 = xmlDoc.SelectSingleNode("//volunteer/address_line_2").InnerText,
                        City = xmlDoc.SelectSingleNode("//volunteer/city").InnerText,
                        State = xmlDoc.SelectSingleNode("//volunteer/state").InnerText,
                        ZipCode = int.Parse(xmlDoc.SelectSingleNode("//volunteer/zip").InnerText),
                        AccountType = xmlDoc.SelectSingleNode("//volunteer/acct_permission").InnerText,
                        AccountStatus = xmlDoc.SelectSingleNode("//volunteer/acct_status").InnerText,
                        CreationDate = DateTime.ParseExact(xmlDoc.SelectSingleNode("//volunteer/cervis_start_date").InnerText,
                        "yyyy-MM-dd", CultureInfo.InvariantCulture),
                        LastActive = DateTime.ParseExact(xmlDoc.SelectSingleNode("//volunteer/last_activity").InnerText,
                        "yyyy-MM-dd", CultureInfo.InvariantCulture)
                    };
                }
                else {
                    log.Error("Survivo volunteer lookup failed for " + email);
                }
            }
        }

        return vol;
    }
}