
(function() {
    'use strict';
    console.log('vCard Creator loaded');

    // Build HTML elements
    var button = $('<a></a>').attr('id', 'vcard').addClass('btn btn-success').text('Download vCard');

    // Scrape data from page
    var head = $('#mainhead');
    var data = $('h2:contains("Contact Info")').parent();
    var names = $('#mainhead').text().trim().replace('Member - ', '').replace(/35[0-9]{4}/, '').trim().split(' ');

    var data_fname = names[0]; names.shift();
    var data_lname = names.join(' ');
    var data_company = $('font#station_title').text().trim();
    var data_title = $(data.children('p:contains("Position:")')[0]).text().trim().replace('Position: ', '').replace('none', 'Member');
    var data_email = $(data.children('p')[0]).text().trim();
    var data_address = $(data.children('p')[1]).text().trim();
    var data_hphone = $(data.children('p')[2]).text().trim().replace(/h: ?/, '');
    var data_cphone = $(data.children('p')[3]).text().trim().replace(/c: ?/, '');

    // Generate data URL
    var mimetype = 'text/x-vcard';
    var fname = encodeURIComponent(data_fname);
    var lname = encodeURIComponent(data_lname);
    var company = encodeURIComponent(data_company);
    var title = encodeURIComponent(data_title);
    var email = encodeURIComponent(data_email);
    //var wphone = encodeURIComponent('123-456-7890');
    var hphone = encodeURIComponent(data_hphone);
    var cphone = encodeURIComponent(data_cphone);
    var address = encodeURIComponent(data_address);
    var page = encodeURIComponent(window.location.href);

    var newline = '%0D%0A';
    var link =
        'data:' + mimetype + ',BEGIN%3AVCARD%0D%0AVERSION%3A3.0' + newline +
        'N%3A' + lname + '%3B' + fname + '%3B%3B%3B' + newline +
        'FN%3A' + fname + '%20' + lname + newline +
        'ORG%3A' + company + '%3B' + newline +
        'TITLE%3A' + title + newline +
        'EMAIL%3Btype%3DINTERNET%3Btype%3DWORK%3Btype%3Dpref%3A' + email + newline +
        //'TEL%3Btype%3DWORK%3Btype%3Dpref%3A' + wphone + newline +
        'TEL%3Btype%3DHOME%3A' + hphone + newline +
        'TEL%3Btype%3DCELL%3A' + cphone + newline +
        'item1.ADR%3Btype%3DHOME%3A%3B%3B' + address + newline +
        'item3.URL%3Btype%3Dpref%3A' + page + newline +
        'END%3AVCARD%0D%0A';

    button.attr('href', link).attr('download', (data_fname + ' ' + data_lname).replace(/ /g, '_').toLowerCase());

    // Add button to page
    var all = outer.append(inner.append(button));
    all.insertAfter('#mainhead');
})();
