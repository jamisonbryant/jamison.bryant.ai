
public class Hacker
{
    /**
     * Returns a random guess from the password list
     *
     * @return Random guess
     */
    public String getGuess()
    {
        guess = passwords.get(0);
        return guess;
    }

    /**
     * Returns an educated guess from the password list
     *
     * @param similarity Similarity of previous guess and correct answer
     * @return Educated guess
     */
    public String getGuess(int similarity)
    {
        // Remove old guess from passwords list
        if (passwords.contains(guess)) {
            passwords.remove(guess);
        }

        for (int i = 0; i < passwords.size(); i++) {
            // Compare old guess to each password
            String password = passwords.get(i);
            int matches = 0;

            for (int j = 0; j < password.length(); j++) {
                if (password.charAt(j) == guess.charAt(j)) {
                    matches++;
                }
            }

            // Check if password and guess are similar
            if (matches != similarity) {
                // Remove password from passwords list
                passwords.remove(password);
            }
        }

        // Get new guess from passwords list
        guess = passwords.get(0);

        // Return new guess
        return guess;
    }
}
