# My Website

My personal website, hosted at https://jamison.bryant.ai.

## Requirements

1. PHP 7.4+ with `intl`, `mbstring`, `bcmath`, and `simplexml` extensions
2. Apache 2.4+ with `mod_rewrite`
3. MariaDB 10.1+
4. Composer 1.8+

## Setup Instructions

### Devilbox (local development)

**Prerequisite 1:** The instructions below were written for use on a Windows system with Docker and WSL2 installed. Many
of the commands will run appropriately on non-Windows systems, however. If you are on a Windows system and do not have
WSL2 installed, first [install WSL2][wsl2].

> **Note about Docker and WSL2:** Docker and the Windows Subsystem for Linux are revolutionary but, as with all projects
> in early stages, take some finagling to get working sometimes. If you're experiencing problems with running the dev
> environment on Windows, check the following things for possible solutions:
>   1. Ensure Windows and WSL are up-to-date (use WSL2)
>   1. Ensure Docker Desktop is up-to-date
>   1. Within your container, ensure you are a member of `docker` group
>   1. Ensure your distro (`wsl.exe -l -v`) is running on WSL2
>   1. Ensure your `docker-desktop` and `docker-desktop-data` are running

**Prerequisite 2:** This project runs in Devilbox, a Docker container pre-baked for LAMP development. If you do not
already have it installed, first [install Devilbox][devilbox].

> **Note:** It is recommended to install Devilbox in your home directory, e.g. at `~/devilbox`. Also, when setting up
> your `.env` file for Devilbox, make sure you set `HOST_PATH_HTTPD_DATA_DIR` to the directory on your host that
> contains your Devilbox projects (`jamison.bryant.ai/` will be a child of this directory).

[wsl2]: https://docs.microsoft.com/en-us/windows/wsl/install-win10
[devilbox]: https://devilbox.readthedocs.io/en/latest/getting-started/install-the-devilbox.html

**0. Clone the repository:**

In your terminal, run this command:

```bash
host> git clone git@gitlab.com:jamisonbryant/jamison.bryant.ai.git
```

**1. Start the container:**

In your terminal, run this command (pass `-d` to run in background):

```bash
host> cd ~/devilbox
host> docker-compose up [httpd php mysql ...]   # arguments in brackets are optional
```

To get a bash prompt inside the docker container at any time, run these commands:

```bash
host> cd ~/devilbox
host> ./shell.sh
```

**2. Visit the web interface:**

In a browser, visit <http://localhost>. You should see the Devilbox intranet control panel.

Click Virtual Hosts in the navbar. You should see the site listed in the table. If there are errors, here is how you
correct them:

#### Problem: Missing htdocs directory in: /path/to/projects/jamison.bryant.ai/

**To fix:** Run this command in your terminal:

```bash
docker> cd jamison.bryant.ai
docker> ln -s webroot/ htdocs
```

#### Problem: No Host DNS record found. Add the following to /etc/hosts: 127.0.0.1 jamison.bryant.ai.loc

**To fix:** Do as the instructions say, add the given line (starting with `127.0.0.1`) to the `hosts` file __on your
development machine__ (NOT the Devilbox!)

**3. Create the database:**

From the Devilbox cpanel, click Tools > phpMyAdmin.

Create a new database called `jamison.bryant.ai`. You can leave all settings the way they are.

**4. Install app dependencies:**

First install PHP dependencies in Composer:

```bash
devilbox> cd jamison.bryant.ai      # this path will now be called JAI_ROOT
devilbox> composer install
```

Now, install static assets and other dependencies using NPM:

```bash
devilbox> cd {{JAI_ROOT}}/webroot
devilbox> npm install
```

**8. Configure app:**

Create a local config file:
```bash
devilbox> cd {{JAI_ROOT}}/config
devilbox> cp .env.dev .env
```

Generate a security salt:
```bash
devilbox> cd {{JAI_ROOT}}
devilbox> ./bin/cake security generate-salt       # copy this to your clipboard for later
```

Update the config file as shown below:
```
# Security configuration
export SECURITY_SALT="$GENERATED_SALT"      # use the salt generated above

# Database configuration
export DB_HOST="127.0.0.1"
export DB_PORT="3306"
export DB_USERNAME="root"
export DB_PASSWORD=""
export DB_NAME="jamison.bryant.ai"
```

**9. Set up database:**

Run migrations:
```bash
devilbox> cd {{JAI_ROOT}}
devilbox> ./bin/cake migrations migrate
```

Create the first user:
```bash
devilbox> ./bin/cake user create-user
```

### Test/Production (AWS)

Deployments to environments above dev are done automatically by GitLab pipelines. No manual deployment instructions are
available at this time. See `RUNBOOK.md` for more details.
