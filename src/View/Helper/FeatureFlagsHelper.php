<?php
namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;

/**
 * FeatureFlags helper
 */
class FeatureFlagsHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Returns true if a feature is enabled in this env, false otherwise
     *
     * @param string $featureName Name of feature to check if enabled
     * @return bool
     */
    public function enabled(string $featureName): bool
    {
        $env = env('APPLICATION_ENV');
        $flag = Configure::read("Features.$featureName.$env");

        return $flag === true;
    }
}
