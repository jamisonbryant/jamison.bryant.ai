<?php

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * Flash Helper
 *
 * Renders flash messages in Bootstrap style. Adapted from FriendsOfCake's bootstrap-ui plugin.
 *
 * @package   App\View\Helper
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. ALl rights reserved.
 * @since     v0.1.5
 * @link      https://github.com/FriendsOfCake/bootstrap-ui
 */
class FlashHelper extends Helper
{
    /**
     * Default config
     *
     * - class: List of classes to be applied to the div containing message
     * - attributes: Additional attributes for the div containing message
     *
     * @var array
     */
    protected $_defaultConfig = [
        'class' => ['alert', 'alert-dismissible', 'fade', 'show'],
        'attributes' => ['role' => 'alert'],
        'element' => 'Flash/default',
    ];

    /**
     * Similar to the core's FlashHelper used to render the message set in FlashComponent::set().
     *
     * @param string $key The [Flash.]key you are rendering in the view.
     * @param array $options Additional options to use for the creation of this flash message.
     *    Supports the 'params', and 'element' keys that are used in the helper.
     * @return string|null Rendered flash message or null if flash key does not exist
     *   in session.
     * @throws \UnexpectedValueException If value for flash settings key is not an array.
     */
    public function render($key = 'flash', array $options = [])
    {
        if (!$this->getView()->getRequest()->getSession()->check("Flash.$key")) {
            return null;
        }

        $stack = $this->getView()->getRequest()->getSession()->read("Flash.$key");
        if (!is_array($stack)) {
            throw new \UnexpectedValueException(sprintf(
                'Value for flash setting key "%s" must be an array.',
                $key
            ));
        }

        if (isset($stack['element'])) {
            $stack = [$stack];
        }

        $out = '';
        foreach ($stack as $message) {
            $message = $options + $message;
            $message['params'] += $this->_config;
            $this->getView()->getRequest()->getSession()->delete("Flash.$key");
            $element = $message['element'];
            $isMatch = preg_match('#Flash/(default|success|error|info|warning)$#', $element, $matches);

            if (strpos($element, '.') === false && $isMatch) {
                $class = $matches[1];
                $class = str_replace(['default', 'error'], ['info', 'danger'], $class);

                if (is_array($message['params']['class'])) {
                    $message['params']['class'][] = 'alert-' . $class;
                }

                $isMatch = preg_match('#primary|secondary|light|dark#', $message['params']['class'], $matches);

                if (is_string($message['params']['class']) && $isMatch) {
                    $message['params']['class'] = $this->_config['class'];
                    $message['params']['class'][] = 'alert-' . $matches[0];
                }

                $element = $this->_config['element'];
            }

            $out .= $this->_View->element($element, $message);
        }

        return $out;
    }
}
