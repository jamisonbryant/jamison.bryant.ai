<?php
namespace App\View\Cell;

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\View\Cell;
use DateTime;
use Laminas\Feed\Reader\Reader;

/**
 * Blog Feed Cell
 *
 * Displays a feed from my blog in a variety of formats.
 */
class BlogFeedCell extends Cell
{
    /**
     * List of valid options that can be passed into this cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * @var string Link to blog RSS feed
     */
    private string $feedLink;

    /**
     * @var string Name of blog
     */
    private string $blogName;

    /**
     * @var string Link to blog
     */
    private string $blogLink;

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
        $this->feedLink = Configure::read('Links.social.blog') . '/feed';
    }

    /**
     * Truncates a string to a specified length, respecting word boundaries.
     *
     * @param string $text Text to truncate
     * @param int $length Length to truncate to
     * @return string
     */
    private function truncatePretty(string $text, int $length): string
    {
        //phpcs:disable
        if (strlen($text) < $length) {
            return $text;
        }

        $text_words = explode(' ', $text);
        $out = null;

        foreach ($text_words as $word) {
            if ((strlen($word) > $length) && $out == null) {
                return substr($word, 0, $length) . '...';
            }

            if ((strlen($out) + strlen($word)) > $length) {
                return $out . '...';
            }

            $out .= ' ' . $word;
        }

        return $out;
        //phpcs:enable
    }

    /**
     * Returns the time difference in a human-friendly string (e.g. "3 days ago")
     *
     * @param DateTime $ago Actual date
     * @param bool $full Render full string, down to hours/minutes/seconds
     * @return string
     */
    private function timeAgoPretty(DateTime $ago, $full = false): string
    {
        //phpcs:disable
        $now = new DateTime();
        $diff = $now->diff($ago);

        // @phpstan-ignore-next-line
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = [
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        ];

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
        //phpcs:enable
    }

    /**
     * Parses the RSS feed and returns an array of formatted objects
     *
     * @return array
     */
    private function parseFeed(): array
    {
        $feed = Reader::import($this->feedLink);

        $this->blogName = $feed->getTitle();
        $this->blogLink = $feed->getLink();

        $entries = [];
        for ($i = 0; $i < $feed->count(); $i++) {
            $entry = [
                'title' => $feed->current()->getTitle(),
                'link' => $feed->current()->getLink(),
                'create_date' => $feed->current()->getDateCreated(),
                'create_date_relative' => $this->timeAgoPretty($feed->current()->getDateCreated()),
            ];

            $content = $feed->current()->getContent();
            $content = strip_tags($content);
            $content = str_replace("\n", ' ', $content);
            $entry['content'] = $content;

            $blurb = $this->truncatePretty($entry['content'], 200);
            $entry['blurb'] = $blurb;

            $cats = Hash::extract($feed->current()->getCategories(), '{n}.term');
            $cats = array_diff($cats, ['Uncategorized']);
            $entry['categories'] = $cats;

            $entries[] = $entry;
            $feed->next();
        }

        return $entries;
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display(string $timespan)
    {
        $articles = $this->parseFeed();

        if ($timespan !== null) {
            $articles = array_filter($articles, function ($v, $k) use ($timespan) {
                return $v['create_date']->getTimestamp() >= strtotime($timespan);
            }, ARRAY_FILTER_USE_BOTH);
        }

        $this->set(compact('articles'));
    }
}
