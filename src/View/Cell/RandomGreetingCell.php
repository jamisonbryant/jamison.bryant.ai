<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * RandomGreeting cell
 */
class RandomGreetingCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * @var array
     */
    private array $greetings = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
        $this->greetings = [
            'Silicon is in my blood.',
            'I used to drive an ambulance. Now I rescue code.',
            'Let\'s talk about code.',
            'Self-taught, self-motivated, self-activated.',
            'I code in slices, not layers.',
            'Rapid development is my specialty.',
            'Full-stack is my passion.',
            'Your legacy code doesn\'t scare me!',
            'I made this site fully-pipelined, source-to-server.',
            'I\'m a PHP and JavaScript artisan.',
            'I build enterprise webapps for the cloud.',
            'I like my jobs like I like my pancakes: full-stack.',
            'Put me on your project and see what develops.',
            'I write reusable code...write it once, use it everywhere.',
            'Fully-licensed Geek-to-English translator.',
            'I make the undecipherable understandable.',
            'I think twice, code once.',
            'I fight for the user.',
            'Clear thinking makes for clear coding.',
            'I get with the program.',
            'Life needs constant refactoring. Embrace it!',
            'Don\'t tell anyone, but I\'d do this for free if I could.',
        ];
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $g = $this->greetings;
        shuffle($g);

        $this->set('greeting', $g[0]);
    }
}
