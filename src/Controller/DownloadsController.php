<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Routing\Router;
use function ByteUnits\bytes;

/**
 * Downloads Controller
 *
 * @package   App\Controller
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. All rights reserved.
 * @since     v0.1.5
 *
 * File created: 01/22/2019 14:11 EST
 */
class DownloadsController extends AppController
{
    /**
     * Initializes the controller
     *
     * @throws \Exception
     * @return mixed
     */
    public function initialize()
    {
        // Initialize parent controller
        parent::initialize();

        // Load components
        $this->loadComponent('RequestHandler');
    }

    /**
     * beforeFilter hook
     *
     * @param Event $event Event being filtered
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Determine which actions unauthorized users can access
        // Required: logout
        $this->Auth->allow(['list']);
    }

    /**
     * Returns a list of all available downloads
     *
     * @return mixed
     */
    public function list()
    {
        // Build downloads array
        $downloads = [
            // Images
            $this->headshot(),

            // Code samples
            $this->cppCodeSample(),
            $this->cSharpCodeSample(),
            $this->javaCodeSample(),
            $this->javascriptCodeSample(),
            $this->phpCodeSample(),
            $this->pythonCodeSample(),
            $this->reactCodeSample(),
            $this->shellCodeSample(),

            // Resumes
            $this->pdfResume(),
            $this->wordResume(),
            $this->htmlResume(),

            // Keys
            $this->sshKey(),
            $this->gpgKey(),

            // Vcards
            $this->defaultVcard(),
        ];

        // Build response element
        $response = [
            'downloads' => $downloads,
        ];

        // Send response to view
        $this->set([
            'response' => $response,
            '_serialize' => ['response'],
        ]);
    }

    /**
     * headshot
     *
     * @return array
     */
    private function headshot()
    {
        return [
            'label' => 'Headshot (image)',
            'description' => 'A thumbnail of my face, in case you need it.',
            'url' => Router::url('/img/headshot.jpg', true),
            'size' => bytes(filesize(IMG_ROOT . 'headshot.jpg'))->format(),
            'sha1' => sha1_file(IMG_ROOT . 'headshot.jpg'),
        ];
    }

    /**
     * cppCodeSample
     *
     * @return array
     */
    private function cppCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'cpp' . DS . 'Listener.cpp';

        return [
            'label' => 'C/C++ Code Sample',
            'description' => 'Some C/C++ code I wrote.',
            'url' => Router::url('/downloads/code_samples/cpp/Listener.cpp', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * cSharpCodeSample
     *
     * @return array
     */
    private function cSharpCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'csharp' . DS . 'survivo_component.cs';

        return [
            'label' => 'C# Code Sample',
            'description' => 'Some C# code I wrote.',
            'url' => Router::url('/downloads/code_samples/csharp/survivo_component.cs', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * javaCodeSample
     *
     * @return array
     */
    private function javaCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'java' . DS . 'Cracker.java';

        return [
            'label' => 'Java Code Sample',
            'description' => 'A Java utility I wrote.',
            'url' => Router::url('/downloads/code_samples/java/Cracker.java', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * javascriptCodeSample
     *
     * @return array
     */
    private function javascriptCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'js' . DS . 'vcard_creator.js';

        return [
            'label' => 'JavaScript Code Sample',
            'description' => 'A JavaScript utility I wrote',
            'url' => Router::url('/downloads/code_samples/js/vcard_creator.js', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * phpCodeSample
     *
     * @return array
     */
    private function phpCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'php' . DS . 'IdentifiableBehavior.php';

        return [
            'label' => 'PHP Code Sample',
            'description' => 'A PHP class I wrote.',
            'url' => Router::url('/downloads/code_samples/php/IdentifiableBehavior.php', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * pythonCodeSample
     *
     * @return array
     */
    private function pythonCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'python' . DS . 'ensure_time_and_ticket.hook';

        return [
            'label' => 'Python Code Sample',
            'description' => 'A git hook I wrote in Python.',
            'url' => Router::url('/downloads/code_samples/python/ensure_time_and_ticket.hook', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * reactCodeSample
     *
     * @return array
     */
    private function reactCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'react' . DS . 'react_navbar.jsx';

        return [
            'label' => 'React Code Sample',
            'description' => 'A React component I wrote.',
            'url' => Router::url('/downloads/code_samples/react/react_navbar.jsx', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * shellCodeSample
     *
     * @return array
     */
    private function shellCodeSample()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'shell' . DS . 'funcs.sh';

        return [
            'label' => 'Shell Code Sample',
            'description' => 'A shell script I wrote.',
            'url' => Router::url('/downloads/code_samples/shell/funcs.sh', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),

        ];
    }

    /**
     * pdfResume
     *
     * @return array
     */
    private function pdfResume()
    {
        return [
            'label' => 'Resume (PDF)',
            'description' => 'My resume in PDF format.',
            'url' => 'https://drive.google.com/file/d/1zD3ujIrcyxk1zYqD2PXgG8hWaC0VzT5B/view?usp=sharing',
            'size' => '???',
            'sha1' => '???',
        ];
    }

    /**
     * wordResume
     *
     * @return array
     */
    private function wordResume()
    {
        return [
            'label' => 'Resume (Word)',
            'description' => 'My resume in Word format.',
            'url' => 'https://drive.google.com/file/d/1sTxxXQFLjC221-ArNTwVLEc6Ci1vDlwk/view?usp=sharing',
            'size' => '???',
            'sha1' => '???',

        ];
    }

    /**
     * @return array
     */
    private function htmlResume()
    {
        return [
            'label' => 'Resume (HTML)',
            'description' => 'My resume in HTML format.',
            'url' => 'https://drive.google.com/file/d/1iUO3IFhfjt6Z5kqG7ed5JzXNnpQW-5D0/view?usp=sharing',
            'size' => '???',
            'sha1' => '???',

        ];
    }

    /**
     * sshKey
     *
     * @return array
     */
    private function sshKey()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'keys' . DS . 'ssh' . DS . 'id_rsa.pub';

        return [
            'label' => 'SSH Public Key',
            'description' => 'My public SSH key. Only download this if you know what it\'s for.',
            'url' => Router::url('/downloads/keys/ssh/id_rsa.pub', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * gpgKey
     *
     * @return array
     */
    private function gpgKey()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'keys' . DS . 'gpg' . DS . 'jamison.gpg';

        return [
            'label' => 'GPG Public Key',
            'description' => 'My public GPG key. Only download this if you know what it\'s for.',
            'url' => Router::url('/downloads/keys/gpg/jamison.gpg', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }

    /**
     * defaultVcard
     *
     * @return array
     */
    private function defaultVcard()
    {
        $filename = WWW_ROOT . 'downloads' . DS . 'vcards' . DS . 'jamison-bryant.vcf';

        return [
            'label' => 'My vCard',
            'description' => 'All my contact details in one convenient place.',
            'url' => Router::url('/downloads/vcards/jamison-bryant.vcf', true),
            'size' => bytes(filesize($filename))->format(),
            'sha1' => sha1_file($filename),
        ];
    }
}
