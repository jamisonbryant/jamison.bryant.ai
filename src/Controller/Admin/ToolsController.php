<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Form\Admin\ConfigForm;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Tools Controller
 *
 * This controller is modelless and is used for encapsulating tool logic.
 *
 * @package   App\Controller
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. ALl rights reserved.
 * @since     v0.1.0
 */
class ToolsController extends AppController
{
    /**
     * @var bool This controller does not use a model
     */
    private $uses = false;

    /**
     * beforeFilter hook
     *
     * @param Event $event Event being filtered
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Determine which actions unauthorized users can access
        $this->Auth->allow();
    }

    /**
     * @return \Cake\Http\Response|void|null
     */
    public function configuration()
    {
        $config = new ConfigForm();

        if ($this->request->is('post')) {
            if ($config->execute($this->request->getData())) {
                $this->Flash->success('Configuration updated successfully.');
            } else {
                $this->Flash->error('An error occurred while updating configuration. No changes made.');
            }
        }

        $this->set('config', $config);
    }
}
