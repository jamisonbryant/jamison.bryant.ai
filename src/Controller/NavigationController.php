<?php

// phpcs:ignoreFile

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Utility\Hash;

/**
 * Navigation Controller
 *
 * This class uses custom logic to generate "link arrays" which represent the
 * content and structure of the application's navigation menus and toolbars.
 * Once constructed, link arrays are published on an API endpoint for consumption
 * by React components to build navigation controls for their respective UIs.
 *
 * This implementation only supports single-level simple link arrays and has
 * limited context sensitivity. Future versions will support nested link arrays
 * (for building menus) and will be fully context- and state-aware.
 *
 * @package   App\Controller
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. All rights reserved.
 * @since     v0.1.0
 *
 * File created: 01/22/2019 14:11 EST
 */
class NavigationController extends AppController
{
    /**
     * Initializes the controller
     *
     * @throws \Exception
     */
    public function initialize()
    {
        // Initialize parent controller
        parent::initialize();
    }

    /**
     * beforeFilter hook
     *
     * @param Event $event
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Determine which actions unauthorized users can access
        // Required: logout
        $this->Auth->allow(['default']);
    }

    /**
     * Returns the link array for the cover page
     */
    public function default()
    {
        // Get parameters
        $url = $this->request->getParam('?.url', null);
        $linkClass = $this->request->getParam('?.link_class', null);
        $activeClass = $this->request->getParam('?.active_class', null);

        // Build base link array
        $links = [
            [
                'label' => 'Home',
                'url' => Router::url('/', true),
            ],
            [
                'label' => 'About',
                'url' => Router::url('/pages/about', true),
            ],
            [
                'label' => 'Blog',
                'url' => Configure::read('Links.social.blog'),
            ],
            [
                'label' => 'Resume',
                'url' => Router::url('/pages/resume', true),
            ],
            [
                'label' => 'Projects',
                'url' => Router::url('/projects/overview', true),
            ],
            [
                'label' => 'Contact',
                'url' => Router::url('/pages/contact', true),
            ],
        ];

        // Apply classes to all links
        // TODO Re-implement this with some Hash and Collections magic
        foreach ($links as $link => $params) {
            if ($params['url'] === $url) {
                $classList = $linkClass . ' ' . $activeClass;
            } else {
                $classList = $linkClass;
            }

            $links = Hash::insert($links, "$link.class_list", $classList);
        }

        // Build response element
        $response = [
            'params' => [
                'url' => $url,
                'link_class' => $linkClass,
                'active_class' => $activeClass
            ],
            'links' => $links,
        ];

        // Send response to view
        $this->set([
            'response' => $response,
            '_serialize' => ['response']
        ]);
    }
}
