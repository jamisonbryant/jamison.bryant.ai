<?php

namespace App\Controller;

use Cake\Event\Event;

/**
 * Users Controller
 *
 * Encapsulates business logic for interpreting request data, taking the appropriate action, and rendering the correct
 * response to the browser. More info at https://book.cakephp.org/3.0/en/controllers.html.
 *
 * @package   App\Controller
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. ALl rights reserved.
 * @since     v0.1.0
 * @link      https://book.cakephp.org/3.0/en/controllers.html
 */
class UsersController extends AppController
{
    /**
     * beforeFilter hook
     *
     * @param Event $event Event being filtered
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Determine which actions unauthorized users can access
        // Required: logout
        $this->Auth->allow(['logout']);
    }

    /**
     * Logs the user in
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error('Invalid username or password, please try again.');
        }
    }

    /**
     * Logs the user out
     *
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
