<?php

namespace App\Log\Engine;

use Cake\Core\Configure;
use Cake\Log\Engine\BaseLog;
use Cake\Log\Log;
use Cake\Log\LogTrait;

class AWSLog extends BaseLog
{
    use LogTrait;

    /**
     * AWSLog constructor
     *
     * @param array $options Array of configuration options
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param string $level Log message level
     * @param string $message Log message text
     * @param array $context Array of log context names (optional)
     */
    public function log($level, $message, array $context = [])
    {
        Log::write($level, $message);

        // If debug mode enabled, log additional info
        if (Configure::read('debug')) {
            Log::write($level, $this->getDebugMessage($context));
        }
    }

    /**
     * @param array $context Context from which to read debug message
     * @return string
     */
    private function getDebugMessage($context)
    {
        if (!strlen($context['service']) > 0) {
            $context['service'] = '(unspecified)';
        }

        return <<<EOT
DEBUG INFORMATION:
service = ${context['service_name']}
exception_type = ${context['exception_type']}
error_code = ${context['error_code']}
response_code = ${context['status_code']}

ERROR MESSAGE:
${context['error_message']}
EOT;
    }
}
