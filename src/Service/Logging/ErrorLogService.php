<?php

namespace App\Service\Logging;

/**
 * Class ErrorLogService
 *
 * This class exists for really, really temporary debugging purposes. It uses PHP's `error_log()` function to append a
 * log message to a log file, that's it. THIS CLASS SHOULD NOT BE USED FOR SERIOUS LOGGING. That's why only three log
 * levels are supported. Seriously. Don't use this unless you're totally desperate.
 *
 * @package App\Service\Logging
 */
class ErrorLogService
{
    public const MESSAGE_TYPE_APPEND_TO_FILE = 3;

    /**
     * @var string
     */
    private $destination;

    /**
     * @param string $destination Filename
     */
    public function __construct(string $destination)
    {
        if (!file_exists($destination)) {
            fopen($destination, 'w');
        }

        $this->destination = $destination;
    }

    /**
     * @param string $msg Message to log
     */
    public function logError($msg)
    {
        error_log("ERROR => $msg" . PHP_EOL, self::MESSAGE_TYPE_APPEND_TO_FILE, $this->destination);
    }

    /**
     * @param string $msg Message to log
     */
    public function logWarning($msg)
    {
        error_log("WARN  => $msg" . PHP_EOL, self::MESSAGE_TYPE_APPEND_TO_FILE, $this->destination);
    }

    /**
     * @param string $msg Message to log
     */
    public function logFatal($msg)
    {
        error_log("FATAL => $msg" . PHP_EOL, self::MESSAGE_TYPE_APPEND_TO_FILE, $this->destination);
    }
}
