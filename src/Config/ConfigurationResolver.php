<?php

namespace App\Config;

use App\Service\Logging\ErrorLogService;
use Aws\Ssm\Exception\SsmException;
use Aws\Ssm\SsmClient;
use Cake\Http\Exception\InternalErrorException;
use Exception;
use Phpfastcache\CacheManager;
use Phpfastcache\Config\ConfigurationOption;
use Phpfastcache\Exceptions\PhpfastcacheSimpleCacheException;
use Phpfastcache\Helper\Psr16Adapter as FastCacheAdapter;
use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException;

/**
 * Class RuntimeConfigurationResolver
 *
 * @package App\Runtime
 */
class ConfigurationResolver
{
    /**
     * @var int
     */
    public const DEFAULT_EXPIRATION = 300;

    /**
     * @var string
     */
    private $appName;

    /**
     * @var string
     */
    private $appEnv;

    /**
     * @var string|null
     */
    private $awsRegion;

    /**
     * @var \Aws\Ssm\SsmClient
     */
    private $ssmClient;

    /**
     * @var FastCacheAdapter
     */
    private $cache;

    /**
     * @var ErrorLogService
     */
    private $logger;

    /**
     * @param string $appName Application name
     * @param string $appEnv Environment name (e.g. "test")
     * @param string|null $awsRegion Region configuration is stored in (e.g. "us-east-2")
     */
    public function __construct(string $appName, string $appEnv, string $awsRegion = null)
    {
        $this->logger = new ErrorLogService(LOGS . 'config-error.log');

        $this->appName = $appName;
        $this->appEnv = $appEnv;
        $this->awsRegion = $awsRegion;

        try {
            // Create path for cached config
            $cachePath = TMP . 'cache' . DS . 'config' . DS . $this->appName . DS . $this->appEnv;
            if (!file_exists($cachePath)) {
                mkdir($cachePath, 0777, true);
            }

            CacheManager::setDefaultConfig(new ConfigurationOption([
                'path' => $cachePath,
            ]));

            $this->cache = new FastCacheAdapter('Files');
        } catch (Exception $e) {
            $this->logger->logError($e->getMessage());
            $this->logger->logWarning('Failed to initialize Files cache adapter. Cache may not work!');
        }

        if ($this->awsRegion !== null) {
            $this->ssmClient = new SsmClient([
                'region' => $this->awsRegion,
                'version' => 'latest',
            ]);
        }
    }

    /**
     * @param string $key Name of key to fetch
     * @param mixed|null $default Default value to return if key value not found
     *
     * @return mixed
     */
    public function fetch(string $key, $default = null)
    {
        $keyPath = $this->getFullKeyPath($key);

        // Check cache first
        if (($fromCache = $this->tryGetFromCache($keyPath)) !== null) {
            return $fromCache;
        }

        // Then check env
        if (($fromEnv = $this->tryGetFromEnv($key)) !== null) {
            $this->setToCache($keyPath, $fromEnv);

            return $fromEnv;
        }

        // Then check AWS
        if (($fromAWS = $this->tryGetFromAWS('/' . $keyPath)) !== null) {
            $this->setToCache($keyPath, $fromAWS);

            return $fromAWS;
        }

        return $default;
    }

    /**
     * @param string $key Name of key to fetch
     *
     * @return string|null
     */
    public function fetchRequired(string $key): ?string
    {
        $value = $this->fetch($key);

        if ($value === null) {
            throw new InternalErrorException("Failed to resolve required parameter '$key'");
        }

        return $value;
    }

    /**
     * @param string $keyPath Full key path (/looks/like/this)
     *
     * @return string|null
     */
    private function tryGetFromCache(string $keyPath): ?string
    {
        if (!$this->cache) {
            return null;
        }

        $cacheKey = $this->hashKey($keyPath);

        try {
            return $this->cache->get($cacheKey);
        } catch (PhpfastcacheSimpleCacheException | InvalidArgumentException $e) {
            return null;
        }
    }

    /**
     * @param string $key Key name (NOT key path)
     *
     * @return string|null
     */
    private function tryGetFromEnv(string $key): ?string
    {
        return env($key);
    }

    /**
     * @param string $keyPath Full key path (/looks/like/this)
     * @param bool $decrypt Should the value from AWS be decrypted automatically? (default: yes)
     *
     * @return string|null
     */
    private function tryGetFromAWS(string $keyPath, $decrypt = true): ?string
    {
        if (!$this->ssmClient) {
            return null;
        }

        try {
            $parameter = $this->ssmClient->getParameter([
                'Name' => $keyPath,
                'WithDecryption' => $decrypt,
            ]);

            $maybeValue = $parameter->search('Parameter.Value');
        } catch (SsmException $e) {
            switch ($e->getAwsErrorCode()) {
                default:
                case 'ParameterNotFound':
                    $this->logger->logWarning("Parameter Store lookup failed for parameter $keyPath");
                    $maybeValue = null;
                    break;
            }
        }

        return $maybeValue;
    }

    /**
     * @param string $key Name of key to get path for
     *
     * @return string
     */
    private function getFullKeyPath(string $key): string
    {
        return sprintf("%s/%s/%s", $this->appName, $this->appEnv, $key);
    }

    /**
     * @param string $key Key or key path to hash
     *
     * @return string
     */
    private function hashKey(string $key): string
    {
        return sha1($key);
    }

    /**
     * @param string $keyPath Path of desired cache key
     * @param mixed $value Value to set
     * @param int $expiration Time (in seconds) to expire the cache key
     * @return void
     */
    private function setToCache(string $keyPath, $value, $expiration = self::DEFAULT_EXPIRATION)
    {
        $cacheKey = $this->hashKey($keyPath);

        try {
            $this->cache->set($cacheKey, $value, $expiration);
        } catch (CacheException $e) {
            $this->logger->logError('Failed to set parameter to cache! App may be SLOW!');
        }
    }
}
