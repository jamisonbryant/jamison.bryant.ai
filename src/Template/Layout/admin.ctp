<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- Metadata -->
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= $this->fetch('title') ?> - Jamison Bryant</title>
        <?= $this->element('favicons') ?>

        <!-- Page stylesheets -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/brands.css" integrity="sha384-1KLgFVb/gHrlDGLFPgMbeedi6tQBLcWvyNUN+YKXbD7ZFbjX6BLpMDf0PJ32XJfX" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/fontawesome.css" integrity="sha384-jLuaxTTBR42U2qJ/pm4JRouHkEDHkVqH0T1nyQXn1mZ7Snycpf6Rl25VBNthU4z0" crossorigin="anonymous">
        <?= $this->Html->css('layouts/admin') ?>

        <!-- View assets -->
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <a href="<?= Cake\Routing\Router::url('/admin/pages/overview', true) ?>" class="masthead-text navbar-brand">
                <?= $this->Html->image('emblem-dark.png', ['class' => 'emblem img img-fluid mr-2']) ?>
                Admin Panel
            </a>
            <button class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse" data-target="#admin-navbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="admin-navbar">
                <ul class="navbar-nav w-100">
                    <li class="nav-item active">
                        <?= $this->Html->link('<i data-feather="chevrons-left"></i> Back to Site', '/', ['class' => 'nav-link', 'escape' => false]) ?>
                    </li>
                    <?= $this->element('Admin/Navbar/usermenu') ?>
                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <?= $this->element('Admin/Navbar/sidebar') ?>
                <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-2">
                    <div class="container-fluid">
                        <h1 class="display-4"><?= $this->fetch('title') ?></h1>
                        <hr />
                        <?= $this->fetch('content') ?>
                    </div>
                </main>
            </div>
        </div>

        <!-- Page scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" integrity="sha256-XfzdiC+S1keia+s9l07y7ye5a874sBq67zK4u7LTjvk=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
        <?= $this->Html->script('feather-icons.js'); ?>
        <?= $this->Html->script('activenav.js'); ?>
    </body>
</html>
