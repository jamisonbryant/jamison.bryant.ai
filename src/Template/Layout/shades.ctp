<!DOCTYPE html>

<html>
    <head>
        <!-- Metadata -->
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= $this->fetch('title') ?></title>
        <?= $this->element('favicons') ?>

        <!-- Page stylesheets -->
        <?= $this->Html->css('layouts/shades'); ?>
        <?= $this->Html->css('brands'); ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/brands.css" integrity="sha384-1KLgFVb/gHrlDGLFPgMbeedi6tQBLcWvyNUN+YKXbD7ZFbjX6BLpMDf0PJ32XJfX" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/fontawesome.css" integrity="sha384-jLuaxTTBR42U2qJ/pm4JRouHkEDHkVqH0T1nyQXn1mZ7Snycpf6Rl25VBNthU4z0" crossorigin="anonymous">

        <!-- View assets -->
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <div class="overlay"></div>
        <div class="masthead">
            <div class="masthead-bg"></div>
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 my-auto">
                        <div class="masthead-content text-white py-5 py-md-0">
                            <?= $this->Html->image('headshot.jpg', ['class' => 'rounded-circle border mb-3',
                                    'width' => 140, 'height' => 140]) ?>
                            <h1 class="mb-3"><?= $this->fetch('title') ?></h1>
                            <?= $this->fetch('content') ?>
                            <p id="thanks-msg" style="color: yellow; display: none;">Thanks, I'll be in touch!</p>
                            <form id="shade-form" method="post">
                                <div class="input-group input-group-newsletter">
                                    <input type="email" class="form-control" placeholder="Enter email..." name="email">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">Notify Me!</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-icons">
            <ul class="list-unstyled text-center mb-0">
                <li class="list-unstyled-item gitlab">
                    <a href="<?= \Cake\Core\Configure::read('Links.social.gitlab') ?>">
                        <i class="fab fa-gitlab"></i>
                    </a>
                </li>
                <li class="list-unstyled-item linkedin">
                    <a href="<?= \Cake\Core\Configure::read('Links.social.linkedin') ?>">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </li>
                <li class="list-unstyled-item stack-overflow">
                    <a href="<?= \Cake\Core\Configure::read('Links.social.stackoverflow') ?>">
                        <i class="fab fa-stack-overflow"></i>
                    </a>
                </li>
                <li class="list-unstyled-item twitter">
                    <a href="<?= \Cake\Core\Configure::read('Links.social.twitter') ?>">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="list-unstyled-item codepen">
                    <a href="<?= \Cake\Core\Configure::read('Links.social.codepen') ?>">
                        <i class="fab fa-codepen"></i>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Page scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <?= $this->element('shade_form_handler'); ?>
    </body>
</html>
