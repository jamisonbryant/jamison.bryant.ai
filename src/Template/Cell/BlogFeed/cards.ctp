<?php

/**
 * @var array $articles
 */

?>

<?php foreach ($articles as $article): ?>
    <div class="card ml-3 mb-2">
        <div class="card-body">
            <h5 class="card-title text-dark"><?= $article['title'] ?></h5>
            <h6 class="card-subtitle mb-2 text-muted"><?= $article['create_date_relative'] ?></h6>
            <p class="card-text text-dark"><?= $article['blurb'] ?></p>
            <a href="<?= $article['link'] ?>" class="card-link text-primary">read more &raquo;</a>
        </div>
    </div>
<?php endforeach; ?>
