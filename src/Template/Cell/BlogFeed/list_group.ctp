<?php

/**
 * @var array $articles
 */

?>

<ul class="list-group list-group-flush bg-transparent">
    <?php foreach ($articles as $article): ?>
        <li class="list-group-item bg-transparent text-light pl-3 border-top-0">
            <h5 class="mb-1">
                <a href="<?= $article['link'] ?>"><?= $article['title'] ?></a>
            </h5>
            <p class="mb-1"><?= $article['blurb'] ?></p>
            <small>
                <span class="font-italic">
                    <?= $article['create_date_relative'] ?>
                </span>
                <span class="mx-1">
                    -
                </span>
                <a href="<?= $article['link'] ?>">read more &raquo;</a>
            </small>
        </li>
    <?php endforeach; ?>
</ul>
