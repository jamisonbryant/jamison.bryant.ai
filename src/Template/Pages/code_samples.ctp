<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Code Samples') ?>
<?php use Cake\Core\Configure; ?>

<?php
    $this->start('script');
    echo $this->Html->script('/node_modules/code-prettify/loader/run_prettify.js');
    echo $this->Html->css('/node_modules/code-prettify/styles/sons-of-obsidian.css');
    $this->end();
?>

<?php $this->start('css'); ?>
<style>
    html {
        scroll-behavior: smooth;
    }
</style>
<?php $this->end(); ?>

<p class="lead">Samples of code I've written, in a variety of languages, with annotations and comments.</p>

<div class="alert alert-secondary text-center" role="alert">
    <em>Jump to:</em>
    <a href="#php">PHP</a> &bull;
    <a href="#js">Javascript</a> &bull;
    <a href="#python">Python</a> &bull;
    <a href="#cpp">C/C++</a> &bull;
    <a href="#csharp">C#</a> &bull;
    <a href="#java">Java</a> &bull;
    <a href="#shell">Shell</a>
</div>

<!-- PHP -->
<h2 id="php">PHP</h2>
<p>This is a class from my <a href="https://gitlab.com/jamisonbryant/cakephp-secureids-plugin" target="_blank">secureids</a>
    plugin for CakePHP. When a new entity is saved to the database, this class generates a unique Base-64 and
    UUID for use as primary keys for the entity. This has a variety of uses, from security to scalability.
    For more info, check out <a href="https://www.youtube.com/watch?v=gocwRvLhDf8" target="_blank">Tom Scott's
    video on the subject</a> on YouTube.</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'php' . DS . 'IdentifiableBehavior.php',
        'repo_url' => 'https://gitlab.com/jamisonbryant/cakephp-secureids-plugin',
        'download_url' => 'https://gitlab.com/jamisonbryant/cakephp-secureids-plugin/raw/master/src/Model/Behavior/IdentifiableBehavior.php',
    ]);
?>

<!-- Javascript -->
<h2 id="js" class="mt-5">Javascript</h2>
<p>I joined a society at one point that had an online portal that allowed all of its members to get in touch with one
    another in case of an emergency. Trouble was, there was no easy way to import the contacts to my phone, so I built
    a bridge: a Javascript userscript that would add a button to the portal and allow me to download the contacts as
    vCards which import in a snap on Android.</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'js' . DS . 'vcard_creator.js',
        'repo_url' => 'https://gitlab.com/jamisonbryant/frrtweaks',
        'download_url' => 'https://gitlab.com/jamisonbryant/frrtweaks/raw/master/src/vcard_creator/vcard_creator.js',
    ]);
?>

<!-- Python -->
<h2 id="python" class="mt-5">Python</h2>
<p>A git commit hook for ensuring I write good commit messages with time and ticket annotations. Commits like
    these make Jira and managers verrrry happy.</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'python' . DS . 'ensure_time_and_ticket.hook',
        'repo_url' => 'https://gitlab.com/jamisonbryant/githooks',
        'download_url' => 'https://gitlab.com/jamisonbryant/githooks/raw/master/commit-msg/ensure_time_and_ticket.hook'
    ]);
?>

<div class="alert alert-secondary text-center mt-5" role="alert">
    <em>Jump to:</em>
    <a href="#php">PHP</a> &bull;
    <a href="#js">Javascript</a> &bull;
    <a href="#python">Python</a> &bull;
    <a href="#cpp">C/C++</a> &bull;
    <a href="#csharp">C#</a> &bull;
    <a href="#java">Java</a> &bull;
    <a href="#shell">Shell</a>
</div>

<!-- C/C++ -->
<h2 id="cpp" class="mt-5">C/C++</h2>
<p>This was a devilishly difficult class I wrote for <a href="https://bit.camp/">Bitcamp 2015</a>. I was attempting
    to make a voice interface for git...it turned out that speech processing is a much more difficult concept
    than I was expecting! Not to mention this was done in an auditorium packed-full of college kids all jabbering at
    once 😺</p>
<p><em>(Wanna know the cool part? This code totally worked on a computer-generated speech sample file, I just couldn't
    get it functional in the loud auditorium.)</em></p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'cpp' . DS . 'Listener.cpp',
        'repo_url' => 'https://gitlab.com/jamisonbryant/hello-git',
        'download_url' => 'https://gitlab.com/jamisonbryant/hello-git/raw/master/src/Listener.cpp',
    ]);
?>

<!-- C# -->
<h2 id="csharp" class="mt-5">C#</h2>
<p>This is a component that I wrote to communicate with an API that pulled organizational volunteer data down into
        a desktop application that made organizing and dispatching volunteers easier. This was a CRAPPY API, guys,
        ok? I might be a little proud of my REST calls here.</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'csharp' . DS . 'survivo_component.cs',
        'repo_url' => 'https://gitlab.com/jamisonbryant/telaviv',
        'download_url' => 'https://gitlab.com/jamisonbryant/telaviv/raw/csharp/TelAviv/Library/Component/CervisComponent.cs',
    ]);
?>

<!-- Java -->
<h2 id="java" class="mt-5">Java</h2>
<p>This is a realllly simple Java app that I wrote to help me solve the hacking minigames in Fallout 4 back when
    I was a new player. I solve them in my head now, but it took some getting used to! (not familiar with Fallout?
    Check out an online version of the minigame <a href="http://mitchellthompson.net/demos/terminal/">here</a> -
    click the red button to begin!)</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'java' . DS . 'Cracker.java',
        'repo_url' => 'https://gitlab.com/jamisonbryant/fohacker',
        'download_url' => 'https://gitlab.com/jamisonbryant/fohacker/raw/master/src/com/criticalheap/fohacker/Hacker.java',
    ]);
?>

<!-- Shell -->
<h2 id="shell" class="mt-5">Shell</h2>
<p>For this sample I dug up some of my old dotfiles that I wrote. I'm a command-line junkie, so I love customizing
    dotfiles. Custom functions, aliases, commands, you name it. If it makes my dev box go faster I'm all ears.</p>
<?php
    echo $this->element('code_sample', [
        'file' => WWW_ROOT . 'downloads' . DS . 'code_samples' . DS . 'shell' . DS . 'funcs.sh',
        'repo_url' => 'https://gitlab.com/jamisonbryant/mydotfiles',
        'download_url' => 'https://gitlab.com/jamisonbryant/mydotfiles/raw/master/scripts/functions.sh',
    ]);
?>

<div class="alert alert-secondary text-center mt-5" role="alert">
    <em>Jump to:</em>
    <a href="#php">PHP</a> &bull;
    <a href="#js">Javascript</a> &bull;
    <a href="#python">Python</a> &bull;
    <a href="#cpp">C/C++</a> &bull;
    <a href="#csharp">C#</a> &bull;
    <a href="#java">Java</a> &bull;
    <a href="#shell">Shell</a>
</div>

<h3 class="text-center mt-5">Wanna see more?</h3>
<p class="lead">Check out my Codepen! Puzzles, scriptlets, & other treasures.</p>
<div class="text-center">
    <?php
        // CodePen
        echo $this->element('icon_button', [
            'icon' => 'fab fa-codepen',
            'url' => Configure::read('Links.social.codepen'),
            'text' => 'Visit CodePen',
            'classes' => 'codepen m-2 btn btn-lg btn-dark']
        );
    ?>
</div>
