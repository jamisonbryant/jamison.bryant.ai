<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Contact') ?>

<p class="lead">
    Need to get in touch with me? I've provided a variety of ways below.
</p>

<div class="row mt-4">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center border rounded p-4" id="connect">
        <h3>
            Connect with me online
            <br>
            <span class="badge badge-success mt-2">PREFERRED</span>
        </h3>
        <p class="my-3">I'm on a bunch of social networks (who isn't these days?) Take your pick of the ones below.</p>
        <?= $this->element('social_media_buttons') ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center border rounded p-4 mt-4" id="direct">
        <h3>Contact me directly</h3>
        <p class="my-3">
            Please only use this for business inquiries or if I'm not responding elsewhere. Thank you!
        </p>
        <p class="my-3 text-center h5">
            Email: <?= $this->Obfuscation->email('jamison@bryant.ai') ?>
        </p>
        <p class="text-muted font-italic small">
            Note: If you can't view the email address above, you may have JavaScript turned off.
            <br>
            Sorry for the inconvenience, I do this to avoid the spambots.
        </p>
    </div>
</div>

