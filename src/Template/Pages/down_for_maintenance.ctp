<?php $this->layout = 'shades' ?>
<?php $this->assign('title', 'Down For Maintenance') ?>

<p class="mb-4">Either I'm performing planned upgrades or something's broken. I've taken my site offline temporarily.
    To be notified when I'm back online, enter your email address below.</p>
