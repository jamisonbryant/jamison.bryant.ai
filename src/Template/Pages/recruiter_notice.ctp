<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Recruiter Notice') ?>

<p class="lead">I have a bit of an unusual background that disqualifies me from certain jobs. Before you contact me
    about a position, read the disclaimers below and make sure I'm a good fit! 👍</p>

<div id="disclaimer" class="card text-center">
    <div class="card-header">
        <h2 class="text-danger">⚠ Disclaimer ⚠</h2>
        <em>(recruiters, please read!!)</em>
    </div>
    <div class="card-body">
        <p class="my-3">
            <h4>I do not have a college degree.</h4>
            I tried for years and years to make engineering school happen, but it just wasn't meant to be. Eventually it
            got to where I wasn't making enough progress for the dough I was shelling out, so I hit the brakes on my B.S.
            and chose to focus on my career. So far things have been going great!
        </p>
        <p style="font-style: italic;">
            For more info, check out the article
            <a href="https://serioussemicolons.wordpress.com/2019/01/22/why-i-quit-college/">Why I Quit College</a>
            on my blog.
        </p>
    </div>
</div>
