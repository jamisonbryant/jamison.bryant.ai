
<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Hobbies') ?>

<p class="lead">This is what I do when I'm not coding. (hint: it's more coding)</p>

<h4 class="text-muted">Ham Radio</h4>
<p>In 2016 I obtained my General-class Amateur Radio Operator license, callsign K3SNR. Since then I've used radios and
    digital telephony to talk to people all over the world, from Baltimore to Barbados, Atlanta to Acapulco. I provide
    radio communications for public-service events such as marathons, parades, and festivals, and I take my radios with
    me anytime I travel to make some rare out-of-state contacts.</p>
<?= $this->Html->link('Learn more about amateur radio', '/projects/ham-radio', ['class' => 'btn btn-md btn-dark btn-accent border mb-3']) ?>

<h4 class="text-muted">Outdoors</h4>
<p>It may seem odd for a code geek, but I LOVE spending time outdoors. If it's the weekend and the weather is nice,
    I can usually be found outside. I love camping, hiking, biking, climbing, exploring, adventuring, and everything
    in between. Only things I haven't done that I want to do are horseback riding (not many horses in Baltimore)
    and hang-gliding (not many hang-gliders either). Cowabunga!</p>
<?= $this->Html->link('Check out my adventure photos', '/photos/tagged/adventures', ['class' => 'btn btn-md btn-dark btn-accent border mb-3']) ?>

<h4 class="text-muted">(Ethical) Hacking</h4>
<p>Several times a year I try to attend local (or sometimes not-so-local) hackathons and test my speed-coding skills.
    I have two guidelines that I follow that make hackathons uber-challenging and uber-fun:
    <ol>
        <li>Don't come up with an idea in advance unless the rules say you have to (encourages ingenuity)</li>
        <li>Try to implement your project in a new language or framework you have little to no experience with</li>
    </ol>
    The result is a crazy fun fast-paced sprint to the finish, and I don't always have a working project at the end. How
    many hackathons I've placed in? Two. How much have I learned all the other times? So, so much.</p>

<?= $this->Html->link('See my hackathon projects', '/projects/tagged/hackathon', ['class' => 'btn btn-md btn-dark btn-accent border mb-3']) ?>
