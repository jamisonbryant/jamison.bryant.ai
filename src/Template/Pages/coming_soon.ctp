<?php $this->layout = 'shades' ?>
<?php $this->assign('title', 'Coming Soon') ?>

<p class="mb-4">I'm working very hard to finish the development of this site. My target launch date is <strong>April
        2019</strong>. To be notified when it is online, enter your email address below.</p>
