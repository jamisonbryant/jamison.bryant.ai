<?php

use Cake\Core\Configure;

$this->layout = 'cover';
$this->assign('title', 'Home');

?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 text-center text-md-left text-lg-left">
        <div class="thumbnail mb-4">
            <?= $this->Html->image('headshot.jpg', ['class' => 'img img-thumbnail rounded-circle']) ?>
        </div>
        <div class="greeting my-4">
            <h1 class="cover-heading">
                Hi, I'm Jamison.
                <small class="text-yellow d-block mt-2">
                    <?= $this->cell('RandomGreeting', [], ['cache' => ['config' => 'cells']])->render() ?>
                </small>
            </h1>
        </div>
        <div class="intro my-4">
            <p class="lead">
                I am a full-stack web developer specializing in PHP and JavaScript development. I taught myself to code
                at 12 and haven't been able to put it down since.
            </p>
        </div>
        <div class="links my-4">
            <?= $this->Html->link('About Me', '/pages/about', ['class' => 'btn btn-md btn-outline-light bg-transparent mr-2 mb-3']) ?>
            <?= $this->Html->link('My Resume', '/pages/resume', ['class' => 'btn btn-md btn-outline-light bg-transparent mr-2 mb-3']) ?>
            <?= $this->Html->link('My Blog', Configure::read('Links.social.blog'), ['class' => 'btn btn-md btn-outline-light bg-transparent mr-2 mb-3']) ?>
            <?php if ($this->FeatureFlags->enabled('projectsModule')): ?>
                <!-- FEATURE FLAG GUARD: projectsModule -->
                <?= $this->Html->link('My Projects', '/projects', ['class' => 'btn btn-md btn-outline-light bg-transparent mr-2']) ?>
            <?php endif; ?>
        </div>
        <div class="statuses pt-3">
            <h4>What's my status?</h4>
            <p>Interested in hiring me? Need a contractor for your project, or a speaker for your conference? Find out if I'm available:</p>
            <h6 class="d-block d-md-inline d-lg-inline mr-2">
                &raquo; Employment status:
                <?php
                    $status = Configure::read('Status.employment');
                    switch ($status) {
                        default:
                        case 'EMPLOYED':
                            echo '<span class="badge badge-success">EMPLOYED</span>';
                            break;
                        case 'LOOKING':
                            echo '<span class="badge badge-primary">LOOKING</span>';
                            break;
                    }
                ?>
            </h6>
            <h6 class="d-block d-md-inline d-lg-inline  mr-2">
                &raquo; Contract status:
                <?php
                $status = Configure::read('Status.contracting');
                switch ($status) {
                    default:
                    case 'BOOKED':
                        echo '<span class="badge badge-warning">BOOKED</span>';
                        break;
                    case 'AVAILABLE':
                        echo '<span class="badge badge-primary">AVAILABLE</span>';
                        break;
                }
                ?>
            </h6>
            <h6 class="d-block d-md-inline d-lg-inline ">
                &raquo; Speaking status:
                <?php
                $status = Configure::read('Status.speaking');
                switch ($status) {
                    default:
                    case 'BOOKED':
                        echo '<span class="badge badge-warning">BOOKED</span>';
                        break;
                    case 'AVAILABLE':
                        echo '<span class="badge badge-primary">AVAILABLE</span>';
                        break;
                }
                ?>
            </h6>
        </div>
        <div class="social my-4 pt-3">
            <h4 class="mb-3">Find me on social media</h4>
            <?= $this->element('social_media_buttons') ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
        <div id="blog-feed" class="mb-4">
            <h3>Latest Blog Articles</h3>
            <?= $this->cell('BlogFeed', ['timespan' => '-6 months'], ['cache' => ['config' => 'cells']])->render('list_group') ?>
        </div>
        <?php if ($this->FeatureFlags->enabled('siteUpdatesModule')): ?>
            <!-- FEATURE FLAG GUARD: siteUpdatesModule -->
            <div id="site-updates" class="mb-4">
                <h3>Site Updates</h3>
                <?= $this->cell('SiteUpdates')->render() ?>
            </div>
        <?php endif; ?>
    </div>
</div>



