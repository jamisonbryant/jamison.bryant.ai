<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Employment Status') ?>
<?php $status = Cake\Core\Configure::read('Status.employment'); ?>

<p class="lead">My current employment status is:</p>

<?php if ($status === 'LOOKING'): ?>
    <div id="looking" class="mt-4 mb-5 text-center">
        <h1 class="text-primary">LOOKING</h1>
        <p>👀 I'm actively seeking a new opportunity.</p>
    </div>
<?php elseif ($status === 'OPEN'): ?>
    <div id="open" class="mt-4 mb-5 text-center">
        <h1 class="text-primary">OPEN</h1>
        <p>🤔 I'm content with where I'm at right now, but I'm open to discussing new opportunities.</p>
    </div>
<?php elseif ($status === 'EMPLOYED'): ?>
    <div id="employed" class="mt-4 mb-5 text-center">
        <h1 class="text-success">EMPLOYED</h1>
        <p>🚫 I'm happy where I'm at right now, and not looking for a new opportunity at the moment.</p>
    </div>
<?php endif; ?>

<div id="disclaimer" class="card text-center">
    <div class="card-header">
        <h2 class="text-danger">⚠ Disclaimer ⚠</h2>
        <em>(recruiters, please read!)</em>
    </div>
    <div class="card-body">
        <h4>I do not have a college degree.</h4>
        <p class="my-3">
            I tried for years to make engineering school happen, but it just wasn't meant to be. Eventually it got to
            where I wasn't making enough progress for the dough I was shelling out, so I hit the brakes on my B.S. and
            chose to focus on my career. So far things have been going great!</p>
        <p class="font-italic">
            For more info, check out the article
            <a href="https://serioussemicolons.wordpress.com/2019/01/22/why-i-quit-college/">Why I Quit College</a>
            on my blog.
        </p>
    </div>
</div>
