<?php

use Cake\Core\Configure;

$this->layout = 'page';
$this->assign('title', 'Resume');

?>

<p class="lead">Download my resume in a variety of formats below.</p>

<div class="row text-center text-muted">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <i class="fas fa-file-pdf fa-8x my-5"></i>
        <?= $this->Html->link('Adobe PDF (.pdf)', Configure::read('Links.resume.pdf'),
            ['class' => 'btn btn-success btn-lg d-block']) ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <i class="fas fa-file-word fa-8x my-5"></i>
        <?= $this->Html->link('Microsoft Word (.docx)', Configure::read('Links.resume.docx'),
            ['class' => 'btn btn-primary btn-lg d-block']) ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <i class="fas fa-file-code fa-8x my-5"></i>
        <?= $this->Html->link('OpenDocument (.odt)', Configure::read('Links.resume.odt'),
            ['class' => 'btn btn-warning btn-lg d-block']) ?>
    </div>
    <div class="dropdown mx-auto mt-4">
        <button class="btn btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown">
            Other formats
        </button>
        <div class="dropdown-menu">
            <?= $this->Html->link('Rich Text (.rtf)', Configure::read('Links.resume.rtf'),
                ['class' => 'dropdown-item']) ?>
            <?= $this->Html->link('EPUB Publication (.epub)', Configure::read('Links.resume.epub'),
                ['class' => 'dropdown-item']) ?>
            <?= $this->Html->link('Compressed HTML (.zip)*', Configure::read('Links.resume.html'),
                ['class' => 'dropdown-item']) ?>
            <?= $this->Html->link('Plain Text (.txt)*', Configure::read('Links.resume.txt'),
                ['class' => 'dropdown-item']) ?>
            <small class="dropdown-item text-muted">* This is auto-generated, so it may be messy!</small>
        </div>
    </div>
    <p class="mt-3 text-muted font-italic">
        Note: If you have a problem viewing a file in your web browser, please download it and open it using the
        appropriate program on your computer.
    </p>
</div>


