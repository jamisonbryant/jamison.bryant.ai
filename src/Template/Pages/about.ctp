<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'About') ?>

<p class="lead">
    I am a senior full-stack developer in Baltimore, MD. I like to write code, design flexible and scalable systems,
    and solve problems.</p>

<h2>Bio</h2>
<p>I was born and raised in the Peach State (that's Georgia), and moved to Maryland in 2008. I got my first job helping
    my dad's software company move at age 15. Once they learned I could program, they hired me on as an intern. I spent
    the next several years in various internships until it was time for college, at which point I started studying to
    get a degree in computer engineering from a four-year university.</p>

<p>Unfortunately, higher education did not agree with me. I was too self-driven. After several honest-but-failed
    attempts at getting the degree, I decided the loans weren't worth the prize, and put my studies on hold.
    Fortunately, my motto has always been <em>"Never Stop Learning"</em>, and so I never stopped studying, tinkering,
    and coding the entire time I was in school. I ended up with plenty of projects, experience, and knowledge that I
    gleaned from my peers.</p>

<p>Shortly after leaving college life behind I was offered a job at a tech startup and although they are no longer
    around, that job truly launched my career. Since then I've had the great fortune to work alongside incredibly
    smart and talented people at organizations <a href="https://quickenloans.com">large</a>
    and <a href="https://orases.com">small</a>, honing and sharpening my skills along the way.</p>

<div class="text-center my-5 card shadow-sm">
    <div class="card-body">
        <h5 class="text-muted mb-3">Want to know more?</h5>
        <?= $this->Html->link('My Outreach', '/pages/outreach', ['class' => 'btn btn-md btn-primary']) ?>
        <?php if ($this->FeatureFlags->enabled('interestsPage')): ?>
            <!-- FEATURE FLAG GUARD: interestsPage -->
            <?= $this->Html->link('My Interests', '/pages/interests', ['class' => 'btn btn-md btn-primary']) ?>
        <?php endif; ?>
        <?= $this->Html->link('My Blog', Cake\Core\Configure::read('Links.social.blog'), ['class' => 'btn btn-md btn-primary']) ?>
    </div>
</div>

<h2>Experience</h2>
<table class="table">
    <tr>
        <th class="text-muted">2005</th>
        <td>
            <p>Taught myself HTML and CSS to help my dad with his non-profit's website.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2007</th>
        <td>
            <p>Learned PHP and MySQL so I could make websites for my friends and clubs I was in.</p>
            <p>Joined the Engineering Exploring Program at Lockheed-Martin and learned
                <a href="https://www.kipr.org/kiss-institute-for-practical-robotics/hardware-software">KISS-C.</a></p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2008</th>
        <td>
            <p>Took Java I, II, and III as well as J2EE I and II at the local community college.</p>
            <p>Started experimenting with Ubuntu and Linux software development.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2009</th>
        <td>
            <p>Got my first software internship writing Java/J2EE at <a href="https://www.drfirst.com/">DrFirst</a>, an
                e-prescribing company.</p>
            <p>Was introduced to <a href="https://cakephp.org">CakePHP</a> by a coworker and instantly fell in love.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2011</th>
        <td>
            <p>Got an internship as a web developer for the <a href="https://www.nist.gov/ncnr">NIST Center for Neutron
                Research</a>.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2012</th>
        <td>
            <p>Enrolled in a computer engineering program at the <a href="https://rit.edu">Rochester Institute of
                Technology</a>. Wasn't a good fit, transferred to community college after one quarter.</p>
            <p>Got an internship at <a href="https://www.roboticresearch.com">Robotic Research, LLC</a>
                working on robots and other secret stuff for the U.S. military</p>
            <p>Taught myself Python and Bash shell scripting, and started using Emacs.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2013</th>
        <td>
            <p>Enrolled in a Computer Engineering program at the <a href="https://umd.edu">University of Maryland</a>.
                Hated that the school was so big, transferred back to community college after one semester.</p>
            <p>Taught myself Sass, Node.js, and jQuery.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2014</th>
        <td>
            <p>Decided to try my hand at creating a startup, and joined forces with my father to create CapitolRX.</p>
            <p>Learned everything I could about servers, DevOps, and enterprise application management.</p>
            <p>Taught myself Vagrant, Phing, TravisCI, Bitbucket, and Jira.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2015</th>
        <td>
            <p>Got an internship and then a full-time position as a Software Developer at
                <a href="https://i-a-i.com">Intelligent Automation, Inc.</a> working on more robots and more secret
                stuff for the U.S. military.</p>
            <p>Learned a crapload of C and C++, and switched to Vim.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2016</th>
        <td>
            <p>Enrolled in a computer engineering program at the
                <a href="https://umbc.edu">University of Maryland, Baltimore County.</a></p>
            <p>Developed my first CakePHP plugin
                (<a href="https://github.com/syntaxera/cakephp-sparkpost-plugin">cakephp-sparkpost-plugin</a>) after
                Mandrill retired their free transactional email service, leaving developers everywhere in the lurch.</p>
            <p>Taught myself Composer, SemVer, and Packagist.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2018</th>
        <td>
            <p>Joined the <a href="https://arbutusvfd.org">Arbutus Volunteer Fire Department</a> and obtained my
                Emergency Medical Technician (EMT) certification.</p>
            <p>Withdrew from UMBC after 1.5 years in pursuit of better opportunities.</p>
            <p>Started work as a Full-Stack Developer at <a href="https://akooba.com">Akooba, Inc.</a></p>
            <p>Taught myself React.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2019</th>
        <td>
            <p>Laid off by Akooba, hired as a Senior Software Developer by
                <a href="https://www.quickenloans.com/">Quicken Loans</a>.</p>
            <p>Got married to the love of my life 💘</p>
            <p>Moved to Detroit, Michigan, USA!</p>
            <p>Learned Angular, Symfony, CircleCI, SonarQube, Twig, and many other technologies.</p>
        </td>
    </tr>
    <tr>
        <th class="text-muted">2020</th>
        <td>
            <p>Decided we missed home, so started looking for jobs back East.</p>
            <p>Welcomed our daughter to the world (won't be long until I can sign her up for coding bootcamp 😉)</p>
            <p>Hired as a Senior Full-Stack Engineer by <a href="https://orases.com">Orases Consulting</a>.</p>
            <p>Moved back to Baltimore, MD to be closer to family.</p>
        </td>
    </tr>
</table>

<?php if (Cake\Core\Configure::read('Status.employment') != 'EMPLOYED'): ?>
    <div class="text-center my-5 card shadow-sm">
        <div class="card-body">
            <h5 class="text-muted mb-3">Looking to hire me?</h5>
            <?= $this->Html->link('View my resume', '/pages/resume', ['class' => 'btn btn-md btn-primary']) ?>
            <?= $this->Html->link('Get in touch', '/pages/contact', ['class' => 'btn btn-md btn-primary']) ?>
        </div>
    </div>
<?php endif; ?>

<h2 class="mt-4">List of My Favorite Things</h2>
<p>Because what about page would be complete without this?!</p>

<table class="table table-striped table-sm">
    <tbody>
        <tr>
            <th>Favorite movie 🎬</th>
            <td><em>Pulp Fiction</em> (Tarantino, 1994)</td>
        </tr>
        <tr>
            <th>Favorite book or novel 📘</th>
            <td><em>Eragon</em>, Book 1 of <em>The Inheritance Cycle</em> - C. Paolini</td>
        </tr>
        <tr>
            <th>Favorite actor 👨‍🎤</th>
            <td><a href="https://en.wikipedia.org/wiki/Hans_Landa">Christoph Waltz</a>, so versatile!</td>
        </tr>
        <tr>
            <th>Favorite actress 👩‍🎤</th>
            <td><a href="https://en.wikipedia.org/wiki/Gwendoline_Christie">Gwendoline Christie</a></td>
        </tr>
        <tr>
            <th>Favorite hero 💪</th>
            <td>Iron Man</td>
        </tr>
        <tr>
            <th>Favorite villain 😠</th>
            <td>Ozymandias 🧠</td>
        </tr>
        <tr>
            <th>Favorite 90s rapper 🎤</th>
            <td>Eminem a.k.a. Marshall Mathers a.k.a. Slim Shady</td>
        </tr>
        <tr>
            <th>Favorite band or artist 🎶</th>
            <td>Daft Punk 🤖🤖</td>
        </tr>
        <tr>
            <th>Favorite programming language 🔣</th>
            <td>PHP 🐘🍰</td>
        </tr>
        <tr>
            <th>Favorite keyboard ⌨</th>
            <td>Razer BlackWidow Chroma</td>
        </tr>
        <tr>
            <th>Favorite shell 🐚</th>
            <td>zsh, The Z-Shell, with <a href="https://github.com/robbyrussell/oh-my-zsh">oh-my-zsh</a></td>
        </tr>
        <tr>
            <th>Favorite video game 🎮</th>
            <td>TESV: Skyrim by Bethesda Game Studios, with a silly amount of mods ⚔👑</td>
        </tr>
    </tbody>
</table>
<p class="mt-4">
    <em>I choose not to list my favorite editor in the list above for fear of starting a flame war. But go Vim ;)</em>
</p>
