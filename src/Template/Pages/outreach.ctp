<?php $this->layout = 'page' ?>
<?php $this->assign('title', 'Outreach') ?>
<link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One" rel="stylesheet">
<style type="text/css">
    #fallen-heading {
        font-family: 'Alfa Slab One', sans-serif;
    }
</style>

<p class="lead">A few words about my community service.</p>

<h2>Ham Radio</h2>
<p>One of my favorite activities is to wake up at 0500 on a Saturday morning, throw my radios in the truck, and run
    downtown to support the comms team at local marathons, charity walks, and other public events. Sometimes I'm on
    foot, and sometimes I just use the rig built into my truck. Either way, it's a fun way to get fresh air and some
    comms practice for The Big One.</p>

<?= $this->element('tagged_photo', ['src' => 'radio.jpg',
    'alt' => 'Me manning the dispatch channels at a search-and-rescue exercise for UMBC.']) ?>

<h2>Community Emergency Response Team</h2>
<p>I'm not as active as I once was when I lived closer to HQ, but I used to run with a great group of guys and girls
    at Montgomery County (MD) CERT. CERT was comprised of civilians who wanted to be able to do more than just stand
    around and film when a disaster occurred or somebody got hurt. We learned how to perform advanced first aid, triage
    large groups of victims, do search-and-rescue, and much more.</p>

<?= $this->element('tagged_photo', ['src' => 'cert.jpg',
    'alt' => 'MC-CERT after a training session on rescuing victims from WMATA Metro trains.']) ?>

<h2>Fire Service</h2>
<p>And finally I am a member of the Baltimore County fire service, serving as a volunteer at a station in southwest
    Catonsville, MD. It has been my distinct honor and pleasure to serve alongside these men and women. I don't need to
    say anything more than that, they already know they're heroes.</p>
<?= $this->Html->image('flag.jpg', ['class' => 'img-thumbnail mx-auto d-block m-4']) ?>
<h1 class="mx-auto d-block display-4" id="fallen-heading">#NeverForgetTheFallen</h1>
<p class="text-center">
    <a href="https://www.firehero.org/donate" class="btn btn-danger mt-3">Donate to the N triple F</a>
</p>
