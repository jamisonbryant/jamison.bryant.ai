<?php $this->layout = 'auth' ?>
<?php $this->assign('title', 'Log In') ?>

<?= $this->Form->create(null, ['class' => 'form-signin']) ?>
    <div class="text-center mb-4">
        <?= $this->Html->image('emblem.png', ['class' => 'mb-4', 'width' => 72, 'height' => 72]) ?>
        <h1 class="h3 mb-3 font-weight-normal">Log In</h1>
        <p>Enter your email and password below or, in the words of the Black Knight, <em>None Shall Pass!</em> 🛡⚔</p>
    </div>
    <div class="text-center mb-4">
        <?= $this->Flash->render() ?>
    </div>
    <div class="form-label-group">
        <?= $this->Form->control('email', ['class' => 'form-control', 'label' => false, 'autocomplete' => false, 'placeholder' => 'Email address']) ?>
    </div>
    <div class="form-label-group">
        <?= $this->Form->control('password', ['class' => 'form-control', 'label' => false, 'autocomplete' => false, 'placeholder' => 'Password']) ?>
    </div>
    <div class="checkbox mb-3">
        <?= $this->Form->checkbox('remember_me', ['value' => true]) ?> Remember me
    </div>
    <?= $this->Form->button('Log In', ['class' => 'btn btn-lg btn-primary btn-block']); ?>
    <p class="mt-5 mb-3 text-muted text-center">&copy; <?= date('Y') ?> Jamison Bryant</p>
<?= $this->Form->end() ?>
