<?php use Cake\Routing\Router; ?>
<?php $path = str_replace('\\', '/', str_replace(WWW_ROOT, '', $file)); ?>

<pre class="prettyprint" style="overflow-x: scroll;">
// <?php echo 'file: ' . $this->Html->link('/' . $path, Router::url($path, true)) ?>

<?= file_get_contents($file) ?>
</pre>

<?php 
    if (isset($repo_url)) {
        echo $this->element('icon_button', [
            'icon' => 'fas fa-database',
            'url' => $repo_url,
            'text' => 'Repo',
            'classes' => 'mr-2 btn btn-sm btn-primary'
        ]);
    }

    if (isset($website_url)) {
        echo $this->element('icon_button', [
            'icon' => 'fas fa-globe',
            'url' => $website_url,
            'text' => 'Website',
            'classes' => 'mr-2 btn btn-sm btn-secondary'
        ]);
    }

    if (isset($download_url)) {
        echo $this->element('icon_button', [
            'icon' => 'fas fa-download',
            'url' => $download_url,
            'text' => 'Download',
            'classes' => 'mr-2 btn btn-sm btn-success'
        ]);
    }  
?>