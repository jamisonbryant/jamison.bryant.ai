<?php

$base = \Cake\Routing\Router::url('/img/favicons', true) . '/';
$suffix = "?v=2";

?>

<link rel="apple-touch-icon" sizes="180x180" href="<?= $base . 'apple-touch-icon.png' . $suffix ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?= $base . 'favicon-32x32.png' . $suffix ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?= $base . 'favicon-16x16.png' . $suffix ?>">
<link rel="manifest" href="<?= $base . 'site.webmanifest' . $suffix ?>">
<link rel="mask-icon" href="<?= $base . 'safari-pinned-tab.svg' . $suffix ?>" color="#3d4780">
<link rel="shortcut icon" href="<?= $base . 'favicon.ico' . $suffix ?>">
<meta name="apple-mobile-web-app-title" content="Jamison Bryant">
<meta name="application-name" content="Jamison Bryant">
<meta name="msapplication-TileColor" content="#3d4780">
<meta name="msapplication-config" content="<?= $base . 'browserconfig.xml' . $suffix ?>">
<meta name="theme-color" content="#ffffff">
