<?php

$text = sprintf('<i class="%s"></i> %s', $icon, $text);
echo $this->Html->link($text, $url, ['escape' => false, 'class' => $classes]);
