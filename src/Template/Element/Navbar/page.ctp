<nav class="navbar navbar-expand-lg navbar-dark">
    <h4 class="masthead-text navbar-brand">
        <a href="<?= Cake\Routing\Router::url('/', true) ?>">
            <?= $this->Html->image('emblem.png', ['class' => 'emblem img border rounded-circle mr-2']) ?>
            Jamison Bryant
        </a>
    </h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-content">
        <ul class="navbar-nav ml-auto">
            <li><?= $this->Html->link('Home', '/', ['class' => 'nav-link p-2']) ?></li>
            <li><?= $this->Html->link('About', '/pages/about', ['class' => 'nav-link p-2']) ?></li>
            <li><?= $this->Html->link('Blog', \Cake\Core\Configure::read('Links.social.blog'), ['class' => 'nav-link p-2']) ?></li>
            <li><?= $this->Html->link('Resume', '/pages/resume', ['class' => 'nav-link p-2']) ?></li>
            <li><?= $this->Html->link('Contact', '/pages/contact', ['class' => 'nav-link p-2']) ?></li>
        </ul>
    </div>
</nav>
