<?= $this->Html->script('wait-for-global.js'); ?>
<canvas id="clickouts-to-blog-chart" width="400" height="200"></canvas>
<script>
    /* global  Chart */
    waitForGlobal("Chart", function() {
        new Chart(document.getElementById('clickouts-to-blog-chart').getContext('2d'), {
            type: 'line',
            data: [{
                x: 10,
                y: 20
            }, {
                x: 15,
                y: 10
            }],
            options: {}
        });
    })
</script>
