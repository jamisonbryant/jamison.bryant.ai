<?= $this->Html->script('wait-for-global.js'); ?>
<canvas id="requests-by-device-type-chart" width="400" height="200"></canvas>
<script>
    /* global  Chart */
    waitForGlobal("Chart", function() {
        new Chart(document.getElementById('requests-by-device-type-chart').getContext('2d'), {
            type: 'line',
            data: [{
                x: 10,
                y: 20
            }, {
                x: 15,
                y: 10
            }],
            options: {}
        });
    });
</script>
