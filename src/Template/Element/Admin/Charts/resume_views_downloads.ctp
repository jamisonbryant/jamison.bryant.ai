<?= $this->Html->script('wait-for-global.js'); ?>
<canvas id="resume-views-downloads-chart" width="400" height="200"></canvas>
<script>
    /* global  Chart */
    waitForGlobal("Chart", function() {
        new Chart(document.getElementById('resume-views-downloads-chart').getContext('2d'), {
            type: 'line',
            data: [{
                x: 10,
                y: 20
            }, {
                x: 15,
                y: 10
            }],
            options: {}
        });
    });
</script>
