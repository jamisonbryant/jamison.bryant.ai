<?php

$htmlTemplate = '<span class="badge badge-%s">%s</span>';

switch ($rank) {
    case 'Admin':
        echo sprintf($htmlTemplate, 'danger', 'ADMIN');
        break;
    case 'Family':
        echo sprintf($htmlTemplate, 'success', 'FAMILY');
        break;
    default:
        echo sprintf($htmlTemplate, 'secondary', 'UNKNOWN');
        break;
}
