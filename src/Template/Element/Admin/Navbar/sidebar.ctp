<?php use Cake\Core\Configure; ?>

<nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="airplay"></i> Overview', '/admin/pages/overview',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="package"></i> Projects', '/admin/projects',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="camera"></i> Files &amp; Photos', '/admin/files',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="git-pull-request"></i> Code Samples', '/admin/code-samples',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="file-text"></i> Resume', '/admin/resume',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="credit-card"></i> vCard', '/admin/vcard',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="briefcase"></i> Employment Status', '/admin/employment-status',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="users"></i> Users', '/admin/users',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="shield"></i> Access &amp; Permissions', '/admin/acl',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="book-open"></i> Reports', '/admin/reports',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="bar-chart-2"></i> Analytics', '/admin/analytics',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="settings"></i> System Status', '/admin/system-status',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="download-cloud"></i> Data Export', '/admin/data-export',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="hard-drive"></i> Cache Management', '/admin/cache-management',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('<i data-feather="tool"></i> Maintenance Mode', '/admin/maintenance-mode',
                ['class' => 'nav-link', 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link('Go to GitLab <i data-feather="external-link"></i>', Configure::read('Links.social.gitlab'),
                ['class' => 'nav-link text-primary', 'escape' => false, 'target' => '_blank']) ?>
        </li>
    </ul>
</nav>


<?= $this->Html->link('Reports', '/admin/reports', ['class' => 'nav-link text-primary']) ?>
