<?php $userRank = $this->request->getSession()->read('Auth.User.rank'); ?>

<li class="nav-item dropdown ml-auto">
    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
        <i data-feather="at-sign"></i> User Menu
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-item-text">
            <small>
                Signed in as <br><b>Jamison Bryant</b>
                <?= $this->element('Admin/Badges/graded_rank_badge', ['rank' => $userRank]); ?>
                <?= $this->Html->link('(log out)', '/users/logout', ['class' => 'ml-1 text-secondary']) ?>
            </small>
        </div>
        <div class="dropdown-divider"></div>
        <?= $this->Html->link('My Profile', '/users/profile', ['class' => 'dropdown-item']) ?>
        <?= $this->Html->link('Settings', '/users/profile/settings', ['class' => 'dropdown-item']) ?>
        <?= $this->Html->link('Help/About', '/pages/help-about', ['class' => 'dropdown-item']) ?>
        <?= $this->Html->link('Back to Main Site', '/', ['class' => 'dropdown-item']) ?>
    </div>
</li>
