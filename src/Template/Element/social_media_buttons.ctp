<?php

use Cake\Core\Configure;

echo $this->Html->css('brands.css');

?>

<div class="text-center text-md-left text-lg-left">
    <?php
        // GitLab
        echo $this->element('icon_button', [
            'icon' => 'fab fa-gitlab',
            'url' => Configure::read('Links.social.gitlab'),
            'text' => 'GitLab',
            'classes' => 'gitlab d-block d-md-inline d-lg-inline w-75 w-md-100 w-lg-100 mx-auto mr-md-2 mb-2 btn btn-sm btn-light'
        ]);

        // LinkedIn
        echo $this->element('icon_button', [
            'icon' => 'fab fa-linkedin',
            'url' => Configure::read('Links.social.linkedin'),
            'text' => 'LinkedIn',
            'classes' => 'linkedin d-block d-md-inline d-lg-inline w-75 w-md-100 w-lg-100 mx-auto m-md-2 mb-2 btn btn-sm btn-light'
        ]);

        // Stack Overflow
        echo $this->element('icon_button', [
            'icon' => 'fab fa-stack-overflow',
            'url' => Configure::read('Links.social.stackoverflow'),
            'text' => 'Stack Overflow',
            'classes' => 'stack-overflow d-block d-md-inline d-lg-inline w-75 w-md-100 w-lg-100 mx-auto m-md-2 mb-2 btn btn-sm btn-light'
        ]);

        // Twitter
        echo $this->element('icon_button', [
            'icon' => 'fab fa-twitter',
            'url' => Configure::read('Links.social.twitter'),
            'text' => 'Twitter',
            'classes' => 'twitter d-block d-md-inline d-lg-inline w-75 w-md-100 w-lg-100 mx-auto m-md-2 mb-2 btn btn-sm btn-light'
        ]);

        // CodePen
        echo $this->element('icon_button', [
            'icon' => 'fab fa-codepen',
            'url' => Configure::read('Links.social.codepen'),
            'text' => 'CodePen',
            'classes' => 'codepen d-block d-md-inline d-lg-inline w-75 w-md-100 w-lg-100 mx-auto ml-md-2 mb-2 btn btn-sm btn-dark'
        ]);
    ?>
</div>

