<?php use Cake\Core\Configure; ?>

<div class="row px-4">
    <div class="col-12 col-md">
        <p>
            <?= $this->Html->image('emblem.png', ['class' => 'emblem mb-2']) ?>
            <small class="d-block mb-2 text-muted">
                Copyright &copy; <?= date('Y') ?> Jamison Bryant. All rights reserved.
            </small>
        </p>
    </div>
    <div class="col-6 col-md">
        <h5>Links</h5>
        <ul class="link-list list-unstyled text-small ml-2">
            <li><?= $this->Html->link('Blog', Cake\Core\Configure::read('Links.social.blog'), ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Twitter', Configure::read('Links.social.twitter'), ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('LinkedIn', Configure::read('Links.social.linkedin'), ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Code Samples', '/pages/code-samples', ['class' => 'text-muted']) ?></li>
        </ul>
    </div>
    <div class="col-6 col-md">
        <h5>About</h5>
        <ul class="link-list list-unstyled text-small ml-2">
            <li><?= $this->Html->link('About Me', '/pages/about', ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Outreach', '/pages/outreach', ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Employment Status', '/pages/employment-status', ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Recruiter Notice', '/pages/recruiter-notice', ['class' => 'text-muted']) ?></li>
        </ul>
    </div>
    <div class="col-6 col-md">
        <h5>Resources</h5>
        <ul class="link-list list-unstyled text-small ml-2">
            <li><?= $this->Html->link('vCard', '/downloads/vcards/jamison-bryant.vcf', ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Resume', '/pages/resume', ['class' => 'text-muted']) ?></li>
            <li><?= $this->Html->link('Contact', '/pages/contact', ['class' => 'text-muted']) ?></li>
        </ul>
    </div>
</div>
