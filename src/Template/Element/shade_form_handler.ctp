<?php use Cake\Core\Configure; ?>

<script type="text/javascript">
    $(document).ready(function() {
        let msg = $('#thanks-msg');
        let form = $('#shade-form');

        form.submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: '<?= Configure::read('Api.Zapier.shadeFormWebhookUrl') ?>',
                type: 'post',
                data: form.serialize()
            }).done(function(data) {
                form.hide();
                msg.show();
            });
        });
    });
</script>

