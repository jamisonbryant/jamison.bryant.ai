<style type="text/css">
    .figure {
        display: table;
        margin: 2em auto;
        width: 80%;
    }
</style>

<figure class="figure">
    <?= $this->Html->image($src, ['alt' => $alt, 'class' => 'figure-img img-fluid rounded']) ?>
    <figcaption class="figure-caption">
        <?= $alt ?>
        <span class="text-right">
<!--            Tags:-->
            <?php
//                if (isset($tags)) {
//                    foreach ($tags as $tag) {
//                        echo $this->Html->tag('span', $tag, ['class' => 'badge badge-secondary mr-1']);
//                    }
//                } else {
//                    echo '<em>none</em>';
//                }
            ?>
        </span>
    </figcaption>
</figure>
