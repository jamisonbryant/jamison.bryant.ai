<?php use Cake\Routing\Router; ?>
<?php $this->layout = 'admin' ?>
<?php $this->assign('title', 'Overview') ?>

<div id="quick-links">
    <h3 class="mb-3">Quick Links</h3>
    <?php
        echo $this->Html->link(
            '<i data-feather="package"></i> Manage Projects',
            '/admin/projects',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );

        echo $this->Html->link(
            '<i data-feather="file"></i> Manage Files',
            '/admin/files',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );

        echo $this->Html->link(
            '<i data-feather="file-text"></i> Manage Resume',
            '/admin/resume',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );

        echo $this->Html->link(
            '<i data-feather="credit-card"></i> Manage vCard',
            '/admin/vcard',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );

        echo $this->Html->link(
            '<i data-feather="users"></i> Manage Users',
            '/admin/users',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );

        echo $this->Html->link(
            '<i data-feather="settings"></i> System Status',
            '/admin/system-status',
            ['escape' => false, 'class' => 'btn btn-secondary btn-lg mr-3 mb-3']
        );
    ?>
</div>
<div id="analytics">
    <h3 class="my-3">Reports &amp; Analytics</h3>
    <div class="row mb-3">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Requests Per Hour</h4>
                    </div>
                    <?= $this->element('Admin/Charts/requests_per_hour'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/requests-per-hour',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Requests by Device Type</h4>
                    </div>
                    <?= $this->element('Admin/Charts/requests_by_device_type'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/requests-by-device-type',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">File Downloads</h4>
                    </div>
                    <?= $this->element('Admin/Charts/file_downloads'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/file-downloads',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Resume Views/Downloads</h4>
                    </div>
                    <?= $this->element('Admin/Charts/resume_views_downloads'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/resume-views-downloads',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Clickouts to Social Networks</h4>
                    </div>
                    <?= $this->element('Admin/Charts/clickouts_to_social_networks'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/clickouts-to-social-networks',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Clickouts to Blog</h4>
                    </div>
                    <?= $this->element('Admin/Charts/clickouts_to_blog'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/clickouts-to-blog',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="h5">Clickouts to Code Sites</h4>
                    </div>
                    <?= $this->element('Admin/Charts/clickouts_to_code_sites'); ?>
                    <div class="btn-group mt-2 d-flex" role="group">
                        <?= $this->Html->link('<i data-feather="refresh-cw"></i> Refresh', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                        <?= $this->Html->link('View Report', '/admin/reports/view/clickouts-to-code-sites',
                            ['class' => 'btn btn-sm btn-outline-secondary']); ?>
                        <?= $this->Html->link('<i data-feather="download"></i> Export .PNG', '#',
                            ['class' => 'btn btn-sm btn-outline-secondary', 'escape' => false]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="status">
    <h3 class="my-3">Recent Activity</h3>
    <p class="lead text-muted">Coming soon!</p>
</div>
