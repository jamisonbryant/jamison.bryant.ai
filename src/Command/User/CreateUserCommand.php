<?php

namespace App\Command\User;

use App\Model\Entity\User;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use GenPhrase\Loader;
use GenPhrase\Password;

/**
 * Create User Command
 *
 * Creates a user
 *
 * @package App\Command
 * @author Jamison Bryant <jamison@bryant.ai>
 * @copyright 2018 Jamison Bryant. All rights reserved.
 * @since v0.1.0
 *
 * Created by PhpStorm 2019-01-10 23:40 EST
 */
class CreateUserCommand extends Command
{
    /**
     * Generates a secure (46-bit) password for new users
     *
     * @return string Generated password (unencrypted)
     */
    private function generatePassword()
    {
        // Register loader
        $loader = new Loader();
        $loader->register();

        // Generate and return password
        // Note: this will generate password with 46 bits of entropy
        $password = new Password();

        return $password->generate(46);
    }

    /**
     * Prints a user's details to the console
     *
     * @param ConsoleIo $io I/O interface
     * @param User $user User entity
     * @return void
     */
    private function printUser($io, $user)
    {
        $userData = [
            'bid' => $user->bid,
            'uuid' => $user->uuid,
            'email' => $user->email,
        ];

        foreach ($userData as $field => $datum) {
            $io->out(sprintf(' * %s: %s', $field, $datum));
        }
    }

    /**
     * Prints validation errors to the console
     *
     * @param ConsoleIo $io I/O interface
     * @param array $errors Array of validation errors
     * @return void
     */
    private function printErrors($io, $errors)
    {
        foreach ($errors as $field => $fieldErrors) {
            foreach ($fieldErrors as $fieldName => $fieldError) {
                $io->error(sprintf(' * %s: %s (rule: %s)', $field, $fieldError, $fieldName));
            }
        }
    }

    /**
     * Executes the command
     *
     * @param Arguments $args Command arguments
     * @param ConsoleIo $io Console IO interface
     * @return int|null Return code
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $users = $this->getTableLocator()->get('Users');

        // Get data for user
        $email = $io->ask('Email:');
        $autoGenPassword = $io->askChoice(
            'Auto-generate password? (recommended)',
            ['Y', 'n'],
            'Y'
        );

        if (strtoupper($autoGenPassword) === 'Y') {
            $password = $this->generatePassword();
            $io->out(sprintf('Generated password: %s', $password));
        } else {
            $password = $io->ask('Password:');
        }

        // Create user entity
        $user = $users->newEntity([
            'email' => $email,
            'password' => $password,
        ]);

        // Save user to database
        if ($users->save($user)) {
            $io->info('The following user was created successfully:');
            $this->printUser($io, $user);

            return 0;
        } else {
            $io->err('Unable to create user. The following errors occurred:');
            $this->printErrors($io, $user->getErrors());

            return 1;
        }
    }
}
