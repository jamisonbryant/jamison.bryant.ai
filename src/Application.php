<?php

namespace App;

use App\Command\Security\GenerateSaltCommand;
use App\Command\User\CreateUserCommand;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;

/**
 * Application Setup Class
 *
 * Defines bootstrapping logic and middleware layers used in the application.
 *
 * @package   App
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. ALl rights reserved.
 * @since     v0.1.0
 */
class Application extends BaseApplication
{
    /**
     * {@inheritDoc}
     */
    public function bootstrap()
    {
        // Call parent to load bootstrap from files.
        parent::bootstrap();

        // Bootstrap CLI
        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        }

        // Load DebugKit plugin (only if debug mode enabled)
        if (Configure::read('debug')) {
            try {
                $this->addPlugin(\DebugKit\Plugin::class);
            } catch (MissingPluginException $e) {
                // Do not halt if the plugin is missing
            }
        }

        // Load secure IDs plugin
        $this->addPlugin('SecureIds');

        // Load reCAPTCHA plugin
        $this->addPlugin('Recaptcha');

        // Load Mailgun plugin
        $this->addPlugin('Mailgun');
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware($middlewareQueue)
    {
        $middlewareQueue
            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(null, Configure::read('Error')))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime'),
            ]))

            // Add routing middleware.
            // Routes collection cache enabled by default, to disable route caching
            // pass null as cacheConfig, example: `new RoutingMiddleware($this)`
            // you might want to disable this cache in case your routing is extremely simple
            ->add(new RoutingMiddleware($this, '_cake_routes_'));

        return $middlewareQueue;
    }

    /**
     * Exposes application commands to the CLI interface
     *
     * @param \Cake\Console\CommandCollection $commands Existing command list
     * @return \Cake\Console\CommandCollection|mixed
     */
    public function console($commands)
    {
        // Add security commands
        $commands->add('security generate-salt', GenerateSaltCommand::class);

        // Add user commands
        $commands->add('user create-user', CreateUserCommand::class);

        return $commands;
    }

    /**
     * @return void
     */
    protected function bootstrapCli()
    {
        try {
            $this->addPlugin('Bake');
        } catch (MissingPluginException $e) {
            // Do not halt if the plugin is missing
        }

        $this->addPlugin('Migrations');
    }
}
