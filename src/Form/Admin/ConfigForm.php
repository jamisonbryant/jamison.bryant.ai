<?php

namespace App\Form\Admin;

use Cake\Core\Configure;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;

/**
 * Config Form
 *
 * Defines schema for the form used to update app config files via the web.
 *
 * @package   App\Form\Admin
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant. ALl rights reserved.
 * @since     v0.1.0
 */
class ConfigForm extends Form
{
    /**
     * @var array
     *
     * Configuration data
     */
    private $config = [];

    /**
     * Class constructor
     */
    public function __construct()
    {
        // Construct parent
        parent::__construct();

        // Fetch available config data
        $this->config = Hash::flatten(Configure::read());
    }

    /**
     * Builds the schema for the form
     *
     * @param Schema $schema Existing schema
     * @return Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        foreach ($this->config as $key => $val) {
            $field = preg_replace('/\./', '::', $key, 1);

            if ($field === $key) {
                $field = 'Misc::' . $field;
            } else {
                $field = ucfirst($field);
            }

            $schema->addField($field, ['type' => 'string']);
        }

        return $schema;
    }

    /**
     * Builds the validator for the form
     *
     * @param Validator $validator Existing validator
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        $validator
            ->add('set1.field1', 'length', [
                'rule' => ['minLength', 10],
                'message' => 'Set1.Field1 required',
            ])
            ->add('set2.field1', 'length', [
                'rule' => ['minLength', 10],
                'message' => 'Set2.Field1 required',
            ])
            ->add('set3.field1', 'length', [
                'rule' => ['minLength', 10],
                'message' => 'Set3.Field1 required',
            ]);

        return $validator;
    }

    /**
     * Processes the form data
     *
     * @param array $data Form data to process
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}
