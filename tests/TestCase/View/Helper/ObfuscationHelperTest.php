<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ObfuscationHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ObfuscationHelper Test Case
 */
class ObfuscationHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\ObfuscationHelper
     */
    public $Obfuscation;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Obfuscation = new ObfuscationHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Obfuscation);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testObfuscateEmail()
    {
        $email = 'test+foo+bar+baz@gmail.co.uk.wiz';
        $obfuscated = $this->Obfuscation->email($email);

        $this->assertNotEquals($email, $obfuscated);
    }
}
