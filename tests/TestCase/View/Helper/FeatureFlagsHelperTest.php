<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\FeatureFlagsHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\FeatureFlagsHelper Test Case
 */
class FeatureFlagsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\FeatureFlagsHelper
     */
    public $FeatureFlags;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->FeatureFlags = new FeatureFlagsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FeatureFlags);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
