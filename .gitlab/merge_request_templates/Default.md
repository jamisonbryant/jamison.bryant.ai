## :rotating_light: Summary

> Summarize the new/changed functionality concisely.

### Highlights or special considerations

> Note any compromises or decisions you would like to point out for reviewers
> to focus on.

### Resolves

> If this MR resolves one or more issues, link them below.

## Changes

### UI changes
<!-- Remove this section if it does not apply -->

> Review your own code first and make comments if appropriate. Explain the why and the what.
> Include a summary of the changes you made. Include screenshots if appropriate.

### API changes
<!-- Remove this section if it does not apply -->

> Review your own code first and make comments if appropriate. Explain the why and the what.
> Include a summary of the changes you made. Include screenshots if appropriate.

### CLI changes

<!-- Remove this section if it does not apply -->

> Review your own code first and make comments if appropriate. Explain the why and the what.
> Include a summary of the changes you made. Include screenshots if appropriate.

## Testing instructions

> Include a step-by-step process for testing your changes. If you added new automated tests,
> explain how to run them.

## Notes

> Add any notes here.

## Pre-merge checklist
**Do NOT remove the ~do-not-merge label until all checkboxes below are checked!**

- [ ] I have tested my code in my development environment
- [ ] I have reviewed my own code and left comments where appropriate
- [ ] I have increased or maintained the unit testing coverage
- [ ] I have increased or maintained the acceptance testing coverage

---

<details>
<summary>Click to expand quick actions</summary>

/label ~needs-reviews ~do-not-merge

</details>
