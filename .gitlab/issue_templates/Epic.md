## :anchor: Epic Summary

> Summarize the epic concisely.

## Why this is important

> Why is this epic important? What goals does it drive up to?

## Affected feature

> Which feature of the application does this epic affect?

## Work breakdown

> Add list (with checkboxes) of associated issues.

## Notes

> Add any notes here.

---

<details>
<summary>Click to expand quick actions</summary>

/label ~epic ~needs-issues

</details>
