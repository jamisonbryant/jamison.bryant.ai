#!/usr/bin/env bash

################################################################################
# AWS EC2 Instance Setup Script
#
# This script fully prepares an AWS EC2 Instance running an Amzn Linux 2 64-bit
# AMI to be able to run this application, including installing Apache, PHP, and
# the CodeDeploy agent for deployments, among other packages.
#
# This script should be run **manually** when creating new EC2 instances.
#
# IMPORTANT NOTE: This script is for installing application runtime (e.g. server)
# dependencies ONLY, not actual application dependencies (e.g. a logger library).
# These dependencies should have already been downloaded and installed as part of
# the CI/CD pipeline.
#
# Version 1.1 ~ Author: jamison@bryant.ai ~ Released 04/05/2020
################################################################################

# Collect some data
IPV4_PUBLIC_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)

# Update package manager
yum -y update

# Set timezone (optional, but recommended)
mv /etc/localtime /etc/localtime.bak
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

# Install CodeDeploy agent
yum install -y ruby wget
cd /home/ec2-user

wget https://aws-codedeploy-us-east-2.s3.amazonaws.com/latest/install
chmod +x ./install
./install auto

systemctl start codedeploy-agent
systemctl enable codedeploy-agent
systemctl is-enabled codedeploy-agent
echo "*** INFO: CodeDeploy agent service has been successfully installed and enabled."

# Install Apache, PHP, and MariaDB
amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
yum install -y httpd mariadb-server

# Set up Vhost
mkdir -p /var/www/html/codedeploy-app
cat <<VHOST >> /etc/httpd/conf.d/vhost-codedeploy-app.conf
LoadModule rewrite_module modules/mod_rewrite.so

<VirtualHost *:80>
    DocumentRoot "/var/www/html/codedeploy-app"

    <Directory /var/www/html/codedeploy-app>
        Options FollowSymLinks
        AllowOverride All
    </Directory>
</VirtualHost>
VHOST

# Start services
systemctl start httpd
systemctl enable httpd
systemctl is-enabled httpd

echo "*** INFO: Apache HTTPD server has been successfully installed and enabled."
echo "*** INFO: Test it by visiting http://${IPV4_PUBLIC_IP} in a web browser."
echo "*** INFO: If the server fails to respond verify that port 80 is allowed inbound!"

systemctl start mariadb
MYSQL_NEW_ROOT_PASSWORD=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
# This is the equivalent of running mysql_secure_installation
mysql -u root <<-EOF
UPDATE mysql.user SET Password=PASSWORD('${MYSQL_NEW_ROOT_PASSWORD}') WHERE User='root';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';
FLUSH PRIVILEGES;
EOF

# We do not leave MySQL running because we should be using RDS
systemctl stop mariadb

# Set webroot file permissions
chmod 2775 /var/www && find /var/www -type d -exec chmod 2775 {} \;
find /var/www -type f -exec chmod 0664 {} \;

# TODO: Set up SSL?

# Install PHP extensions
yum install -y php-mbstring php-intl php-xml
php -m
systemctl stop php-fpm
systemctl start php-fpm

# Write phpinfo file
WEBROOT_PATH=$(grep -i '^DocumentRoot' /etc/httpd/conf/httpd.conf | cut -d '"' -f2)
PHPINFO_FILE_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)-phpinfo.php
echo "<?php phpinfo(); ?>" > "${WEBROOT_PATH}/${PHPINFO_FILE_NAME}"

echo "*** INFO: PHP 7.2 has been successfully installed and enabled."
echo "*** INFO: phpinfo file was written to ${WEBROOT_PATH}/${PHPINFO_FILE_NAME}"
echo "*** INFO: Test it by visiting http://${IPV4_PUBLIC_IP}/${PHPINFO_FILE_NAME} in a web browser."
