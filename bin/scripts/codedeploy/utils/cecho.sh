#!/bin/bash

################################################################################
# cecho (Colorful Echo) Utility
#
# Thanks and credit to: "Tux" (https://bytefreaks.net/author/bob)
# https://bytefreaks.net/gnulinux/bash/cecho-a-function-to-print-using-different-colors-in-bash
################################################################################

# The following function prints a text using custom color
#   -c or --color define the color for the print. See the array colors for the available options.
#   -n or --noline directs the system not to print a new line after the content.
# Last argument is the message to be printed.
cecho() {
    declare -A colors;
    colors=(\
        ['black']='\E[0;47m'\
        ['red']='\E[0;31m'\
        ['green']='\E[0;32m'\
        ['yellow']='\E[0;33m'\
        ['blue']='\E[0;34m'\
        ['magenta']='\E[0;35m'\
        ['cyan']='\E[0;36m'\
        ['white']='\E[0;37m'\
    );

    local default_message="No message passed.";
    local default_color="black";
    local default_newline=true;

    while [[ $# -gt 1 ]];
    do
    key="$1";

    case $key in
        -c|--color)
            color="$2";
            shift;
        ;;
        -n|--noline)
            newline=false;
        ;;
        *)
            # unknown option
        ;;
    esac
    shift;
    done

    message=${1:-$default_message};
    color=${color:-$default_color};
    newline=${newline:-$default_newLine};

    echo -en "${colors[$color]}";
    echo -en "$message";
    if [ "$newLine" = true ] ; then
        echo;
    fi

    # Reset text attributes to normal without clearing screen.
    tput sgr0;

    return;
}

print_warning() {
    cecho -c 'yellow' "$@\n";
}

print_error() {
    cecho -c 'red' "$@\n";
}

print_info() {
    cecho -c 'cyan' "$@\n";
}

print_success() {
    cecho -c 'green' "$@\n";
}
