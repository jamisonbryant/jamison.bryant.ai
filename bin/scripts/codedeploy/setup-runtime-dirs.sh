#!/usr/bin/env bash

################################################################################
# CodeDeploy Application Runtime Directories Configurator
#
# Creates directories for the application's logs and filesystem caching, then
# configures their permissions. This is temporary while we work on moving these
# resources to their respective AWS services. This script will be run as part
# of the CodeDeploy AfterInstall lifecycle hook.
#
# Version 1.0 ~ Author: jamison@bryant.ai ~ Released 04/09/2020
################################################################################

cd /var/www/html/codedeploy-app
mkdir tmp
mkdir logs

# From here: https://book.cakephp.org/3/en/installation.html#permissions
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:${HTTPDUSER}:rwx tmp
setfacl -R -d -m u:${HTTPDUSER}:rwx tmp
setfacl -R -m u:${HTTPDUSER}:rwx logs
setfacl -R -d -m u:${HTTPDUSER}:rwx logs
