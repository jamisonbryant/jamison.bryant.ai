#!/usr/bin/env bash

################################################################################
# CodeDeploy Application Server Starter
#
# Starts the application runtime server hosting the application on an AWS EC2
# instance. This script will be run as part of the AWS CodeDeploy lifecycle hook
# BeforeInstall. See /appspec.yml for more information.
#
# Version 1.0 ~ Author: jamison@bryant.ai ~ Released 04/05/2020
################################################################################

sudo systemctl start httpd
