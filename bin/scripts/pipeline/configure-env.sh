#!/usr/bin/env bash

################################################################################
# Environment Configurator
#
# Copies the template environment configuration file and does just the right
# amount of tweaks needed to get the application running.
#
# Version 1.0 ~ Author: jamison@bryant.ai ~ Released 04/11/2020
################################################################################

if ! [[ -f "./index.php" ]]; then
    echo "Error: Must be run from project root (directory containing index.php)"
    exit 1
fi

cd config
cp ".env.${JOB_APPLICATION_ENV}" .env

# Replace required parameters
sed -i "s/___APPLICATION_NAME___/${CI_APPLICATION_NAME}/g" .env

echo "Environment configuration file created in config/.env"
