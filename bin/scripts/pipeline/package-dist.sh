#!/usr/bin/env bash

################################################################################
# Distributable Package Creator
#
# Packages the application source code into a well-named .ZIP file for uploading
# to an AWS S3 bucket, where it can then be picked up by a variety of services
# and uploaded/installed to its final destination. Must be run from root dir.
#
# Application files will be archived as "$DATE_$TIME_$VERSION.zip" and will be
# placed in the dist/ directory off the root dir. This directory should be
# ignored by VCS.
#
# Version 1.0 ~ Author: jamison@bryant.ai ~ Released 04/05/2020
################################################################################

if ! [[ -f "./index.php" ]]; then
    echo "Error: Must be run from project root (directory containing index.php)"
    exit 1
fi

mkdir -p "./dist/"

date=$(date +%Y.%m.%d_%H.%M.%S%Z)
version=$(cat VERSION.md)

if [ -z "$date" ] || [ -z $version ]; then
    echo "Error: Unable to build archive name."
    exit 1
fi

archive_path="dist/${date}_${version}.zip"
zip -r "$archive_path" ./

echo "Application packaged to $archive_path."
