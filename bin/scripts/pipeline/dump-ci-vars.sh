#!/usr/bin/env bash

################################################################################
# Pipeline Var Dumper
#
# Dumps CI-related variables to a file for pipeline debugging.
#
# Version 1.0 ~ Author: jamison@bryant.ai ~ Released 04/11/2020
################################################################################

if ! [[ -f "./index.php" ]]; then
    echo "Error: Must be run from project root (directory containing index.php)"
    exit 1
fi

mkdir -p .sensitive/pipeline

cat <<EOT >> .sensitive/pipeline/ci-vars.txt
    CI_COMMIT_BEFORE_SHA=$CI_COMMIT_BEFORE_SHA
    CI_JOB_ID=$CI_JOB_ID
    CI_JOB_NAME=$CI_JOB_NAME
    CI_COMMIT_SHA=$CI_COMMIT_SHA
    CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME
    CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
    CI_REPOSITORY_URL=$CI_REPOSITORY_URL
    CI_JOB_STAGE=$CI_JOB_STAGE
    CI_JOB_TOKEN=$CI_JOB_TOKEN
    EOT

echo "Pipeline var dump created on .sensitive/pipeline/ci-vars.txt"
