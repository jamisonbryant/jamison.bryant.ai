# Runbook

## Deployments

### First-time Database Setup

1. Remote into EC2 instance with access to RDS endpoint
1. Make sure instance has an elastic IP, copy it's private (AWS) IP
1. Generate a secure password in a separate browser tab
1. Log into MySQL prompt with master credentials and run this SQL:

```sql
CREATE DATABASE jai_app;
CREATE USER 'jai_app_user'@'$ELASTIC_IP_PRIVATE' IDENTIFIED BY '$SUPER_SECRET_PW';
GRANT SELECT, INSERT, UPDATE, DELETE ON jai_app.* TO 'jai_app_user'@'$ELASTIC_IP_PRIVATE';
FLUSH PRIVILEGES;
```

### Migrating the Database (architect only)

1. Copy `.env` file, add RDS master credentials
1. Ensure RDS privilege tables allows remote access from your IP
1. Run `./bin/cake migrations migrate --connection=migrations`

## SDLC

### Merging MRs (architect only)

1. Ensure all pre-merge checkboxes are checked
1. Ensure ~do-not-merge and any other outdated labels have been removed
1. Ensure pipeline is passing, or if not there is a good reason why not
1. Check *Delete source branch* and *Squash commits* checkboxes
1. Click Merge

### Tagging

1. Ensure all MRs that are "going" are merged
1. Check out master: `git checkout master`
1. Ensure master is up to date: `git pull`
1. Ensure version number is up to date: `cat VERSION.md`
1. Create the tag: `git tag -a vX.Y.Z -m "Tagging vX.Y.Z"`
1. Push the tag: `git push --tags`
1. (optional) Create a release


